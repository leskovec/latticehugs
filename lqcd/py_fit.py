import numpy as np
import math as mp
from scipy import optimize

def mk_correl(tdel, Nkonf, eigval,  eigval_avg):
    eval_correl = np.zeros((tdel,tdel),dtype=np.float64)
    for konf in range(Nkonf):
        for t1 in range(tdel):
            for t2 in range(tdel):
                eval_correl[t1,t2] += (eigval[konf,t1] - eigval_avg[t1])*(eigval[konf,t2] - eigval_avg[t2])
    return eval_correl


def chi2_exp(p,y,sigmay,f,inv_cov,tdel):
    c2_sum=0.0e0
    for t1 in range(tdel):
        for t2 in range(tdel):
            c2_sum += (f(t1,p) - y[t1])*inv_cov[t1,t2]*(f(t2,p) - y[t2])\
            /(sigmay[t1]*sigmay[t2])
    ndf=float(tdel-len(p))
    return c2_sum/ndf

def f_one_exp(x, p):
    return p[0]*np.exp(-p[1]*x)

def f_two_exp(x, p):
    return p[0]*np.exp(-p[1]*x) + p[2]*np.exp(-p[3]*x)

def fit_eigenvalue(fit_type, Nkonf, tmin, tmax, P_init, eigval, C_eigval_cv, C_eigval_jack):
    tdel = tmax-tmin+1
    
    eCor = mk_correl(tdel,Nkonf,eigval,C_eigval_cv)
    
    sigma=np.zeros(tdel,dtype=np.float64)
    for t in range(tdel):
        sigma[t] = np.sqrt(eCor[t,t])

    Cor=np.zeros((tdel,tdel),dtype=np.float64)
    for t1 in range(tdel):
        for t2 in range(tdel):
            Cor[t1,t2]=eCor[t1,t2]/(sigma[t1]*sigma[t2])

    invCor=np.linalg.pinv(Cor)
        
    if fit_type == '1exp':           
        #minimize chi2
        args = C_eigval_jack, sigma, f_one_exp, invCor, tdel
        res = optimize.minimize(chi2_exp, P_init,args=args,method='Nelder-Mead')
    elif fit_type == '2exp':               
        #minimize chi2
        args = C_eigval_jack, sigma, f_two_exp, invCor, tdel
        res = optimize.minimize(chi2_exp, P_init,args=args,method='Nelder-Mead')
    if res.success==True:
        chi2=res.fun
        out=res
        succ=True
    else:
        chi2=0.0
        out=res
        succ=False
    return chi2,out,succ
