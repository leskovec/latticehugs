'''
  This python module contains the jackknife routines
'''

import numpy as np


def jack_average_scalar( x):
  #calculates central value
  n = x.shape[0]
  return np.sum(x)/n

def jack_average_vector( x):
  n = x.shape[0]
  nt = x.shape[1]
  avg = np.zeros(nt,dtype=np.complex)
  
  for t in range(nt):
    avg[t]=np.sum(x[:,t])/n
  return avg

def jack_sigma_scalar( x):
  #calculates the jackknife uncertainty
  n = x.shape[0]
  x_avg = np.sum(x)/n
  sigma_x2 = np.sum(np.power(np.add(x, - x_avg),2))
  sigma = np.sqrt(sigma_x2)*np.sqrt((n-1.)/n)
  return sigma

def jack_sigma_vector( x):
  n = x.shape[0]
  nt = x.shape[1]
  x_avg = np.zeros(nt,dtype=np.complex)
  sigma_x2 = np.zeros(nt,dtype=np.complex)
  sigma = np.zeros(nt,dtype=np.complex)

  for t in range(nt):
    x_avg[t] = np.sum(x[:,t])/n
    sigma_x2[t] = np.sum(np.power(np.add(x[:,t], - x_avg[t]),2))
    sigma[t] = np.sqrt(sigma_x2[t])*np.sqrt((n-1.)/n)
  
  return sigma

def jack_resample_scalar(x):
  #jackknife resampling - default bin size is 1
  n = x.shape[0]
  x_J = np.zeros_like(x)
  for iextr in range(n):
    for i in range(n):    
        if iextr != i:
            x_J[iextr] = x_J[iextr] + x[i]
  x_J=x_J/(n-1.0)
  return x_J

def jack_resample_vector(x):
  n = x.shape[0]
  nt = x.shape[1]
  x_J = np.zeros_like(x)
  for iextr in range(n):
    for i in range(n):    
      if iextr != i:
        for t in range(nt):
          x_J[iextr,t] = x_J[iextr,t] + x[i,t]/float(n-1.)
  return x_J
