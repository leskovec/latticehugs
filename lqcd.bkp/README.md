# README #

 Lattice QCD analysis scripts in python.  

 Also kind of an evolving repository. Will need to clean  it up at a certain point and make it consistent, but for now it works and produces correct results (as checked to previous versions who were checked with other folks).

## the inverse Luescher

In any script doing the inverse Luescher you need the following libraries from this package:

1. lqcd.basic : has the required definitions to hold data structures. Or rather it defines a recursive dictionaty that is easy to index.
2. lqcd.plotting : defines plots and various matplotlib stuff. Should be initialized with   init_plotting(8,6), where 8 and 6 refer to plot size in inches. I dont know why inches but ...
3. lqcd._zeta : contains the generalized Zeta function
4. lqcd.c_Tmat_scattering : contains various quantities used in the Tmat fits. 
5. scipy.optimize : for solvers, specifically root

You will also need: matplotlib, python, numpy and scipy


Note: this is currently still under development. A lot of the functions that are in the sample script should already be in c_Tmat_scattering, but are just not there yet.

Installing the packages.

1. nothing need be done - its python
2. nothing need be dong - its python
3. Generalized Zeta function coded up in C and located in the gen_zeta subdirectory. Requires GSL to be installed. Also requires the Intel C compilers (because I check everything with those and they are free for students anyway). The C code is wrapped up in a wrapper function called from python. To compile the python module run python setup_linux.py build_ext --inplace and copy the _zeta.so to the the main directory.
4. c_Tmat_scattering is cython code which is still in development. To compile this part of the code go to the c_Tmat_scatt directory and run
LDSHARED="icc -shared" CC="icc -xhost" CXX="icpc -xhost"  python setup.py build_ext --inplace
(again development, so compilers not coded in the setup.py) note that this is also written in INDEX for convenience. Copy the .so file into the main directory to make it work.

Once these packages are compiled you take a look at the example script for the inverse Luescher.


TODO:  
 - list of modules and their uses  
 - documentation  
 - full pledged package  
 - interfaces  
 - porting to py3.5