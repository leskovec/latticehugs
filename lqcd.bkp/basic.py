from collections import defaultdict
import numpy as np

def ndec(x, offset=2):
  ans = offset - np.log10(x)
  ans = int(ans)
  if ans > 0 and x * 10. ** ans >= [0.5, 9.5, 99.5][offset]:
    ans -= 1
  return 0 if ans < 0 else ans

def StrErr_fmt(v,dv,sdv=0):
  # special cases
  if np.isnan(v) or np.isnan(dv) or np.isnan(sdv):
    return '%g +- %g'%(v, dv)
  elif dv == float('inf'):
    return '%g +- inf' % v
  elif v == 0 and (dv >= 1e5 or dv < 1e-5 or sdv>=1e6 or sdv<1e-5):
    if dv == 0:
      return '0(0)'
    else:
      ans = ("%.1e" % dv).split('e')
      sans = ("%.1e" % sdv).split('e')
      return "0.0(" + ans[0] + ")e" + ans[1]+"(" + sans[0] + ")e" + sans[1]
  elif v == 0:
    if dv >= 9.95:
      return '0(%.0f)(%.0f)' % (dv,sdv)
    elif dv >= 0.995:
      return '0.0(%.1f)(%.1f)' % (dv,sdv)
    else:
      ndecimal = ndec(dv)
      sndecimal = ndec(sdv)
      return '%.*f(%.0f)(%.0f)' % (ndecimal, v, dv * 10. ** ndecimal, sdv*10.**sndecimal)
  elif dv == 0:
    ans = ('%g' % v).split('e')
    if len(ans) == 2:
      return ans[0] + "(0)e" + ans[1]
    else:
      return ans[0] + "(0)"
  elif dv < 1e-6 * abs(v):
    return '%g +- %.2g +- %.2g' % (v, dv, sdv)
  elif dv > 1e4 * abs(v):
    return '%.1g +- %.2g +- %.2g' % (v, dv, sdv)
  elif abs(v) >= 1e6 or abs(v) < 1e-7:
    exponent = np.floor(np.log10(abs(v)))
    fac = 10.**exponent
    mantissa = str(v/fac)
    exponent = "e" + ("%.0e" % fac).split("e")[-1]
    return mantissa + exponent

  # normal cases
  if dv >= 9.95:
    if abs(v) >= 9.5:
      return '%.0f(%.0f)(%.0f)' % (v, dv,sdv)
    else:
      ndecimal = ndec(abs(v), offset=1)
      return '%.*f(%.*f)(%.*f)' % (ndecimal, v, ndecimal, dv, ndecimal, sdv)
  if dv >= 0.995:
    if abs(v) >= 0.95:
      return '%.1f(%.1f)(%.1f)' % (v, dv,sdv)
    else:
      ndecimal = ndec(abs(v), offset=1)
      return '%.*f(%.*f)(%.*f)' % (ndecimal, v, ndecimal, dv,ndecimal,sdv)
  else:
    ndecimal = max(ndec(abs(v), offset=1), ndec(dv) )
    return '%.*f(%.0f)(%.0f)' % (ndecimal, v, dv * 10. ** ndecimal, sdv * 10. ** ndecimal)

def StrErrTwoSys_fmt(v,dv,sdv1=0,sdv2=0):
  # special cases
  if np.isnan(v) or np.isnan(dv) or np.isnan(sdv1) or np.isnan(sdv2):
    return '%g +- %g'%(v, dv)
  elif dv == float('inf'):
    return '%g +- inf' % v
  elif v == 0 and (dv >= 1e5 or dv < 1e-5 or sdv1>=1e6 or sdv1<1e-5 or sdv2>=1e6 or sdv2<1e-5):
    if dv == 0:
      return '0(0)'
    else:
      ans = ("%.1e" % dv).split('e')
      sans = ("%.1e" % sdv1).split('e')
      sans2 = ("%.1e" % sdv2).split('e')
      return "0.0(" + ans[0] + ")e" + ans[1]+"(" + sans[0] + ")e" + sans2[1] +"(" + sans2[0] + ")e" + sans2[1]
  elif v == 0:
    if dv >= 9.95:
      return '0(%.0f)(%.0f)(%.0f)' % (dv,sdv1,sdv2)
    elif dv >= 0.995:
      return '0.0(%.1f)(%.1f)(%.1f)' % (dv,sdv1,sdv2)
    else:
      ndecimal = ndec(dv)
      sndecimal = ndec(sdv1)
      s2ndecimal = ndec(sdv2)
      return '%.*f(%.0f)(%.0f)(%.0f)' % (ndecimal, v, dv * 10. ** ndecimal, sdv1*10.**sndecimal, sdv2*10.**s2ndecimal)
  elif dv == 0:
    ans = ('%g' % v).split('e')
    if len(ans) == 2:
      return ans[0] + "(0)e" + ans[1]
    else:
      return ans[0] + "(0)"
  elif dv < 1e-6 * abs(v):
    return '%g +- %.2g +- %.2g +- %.2g' % (v, dv, sdv1,sdv2)
  elif dv > 1e4 * abs(v):
    return '%.1g +- %.2g +- %.2g +- %.2g' % (v, dv, sdv1,sdv2)
  elif abs(v) >= 1e6 or abs(v) < 1e-7:
    exponent = np.floor(np.log10(abs(v)))
    fac = 10.**exponent
    mantissa = str(v/fac)
    exponent = "e" + ("%.0e" % fac).split("e")[-1]
    return mantissa + exponent

  # normal cases
  if dv >= 9.95:
    if abs(v) >= 9.5:
      return '%.0f(%.0f)(%.0f)(%.0f)' % (v, dv,sdv1,sdv2)
    else:
      ndecimal = ndec(abs(v), offset=1)
      return '%.*f(%.*f)(%.*f)(%.*f)' % (ndecimal, v, ndecimal, dv, ndecimal, sdv1, ndecimal, sdv2)
  if dv >= 0.995:
    if abs(v) >= 0.95:
      return '%.1f(%.1f)(%.1f)(%.1f)' % (v, dv,sdv1, sdv2)
    else:
      ndecimal = ndec(abs(v), offset=1)
      return '%.*f(%.*f)(%.*f)(%.*f)' % (ndecimal, v, ndecimal, dv, ndecimal, sdv1, ndecimal, sdv2)
  else:
    ndecimal = max(ndec(abs(v), offset=1), ndec(dv) )
    return '%.*f(%.0f)(%.0f)(%.0f)' % (ndecimal, v, dv * 10. ** ndecimal, sdv1 * 10. ** ndecimal, sdv2 * 10. ** ndecimal)

def StrErrNoSys_fmt(v,dv):
  # special cases
  if np.isnan(v) or np.isnan(dv):
    return '%g +- %g'%(v, dv)
  elif dv == float('inf'):
    return '%g +- inf' % v
  elif v == 0 and (dv >= 1e5 or dv < 1e-5):
    if dv == 0:
      return '0(0)'
    else:
      ans = ("%.1e" % dv).split('e')
      return "0.0(" + ans[0] + ")e" + ans[1]
  elif v == 0:
    if dv >= 9.95:
      return '0(%.0f)' % (dv)
    elif dv >= 0.995:
      return '0.0(%.1f)' % (dv)
    else:
      ndecimal = ndec(dv)
      return '%.*f(%.0f)' % (ndecimal, v, dv * 10. ** ndecimal)
  elif dv == 0:
    ans = ('%g' % v).split('e')
    if len(ans) == 2:
      return ans[0] + "(0)e" + ans[1]
    else:
      return ans[0] + "(0)"
  elif dv < 1e-6 * abs(v):
    return '%g +- %.2g' % (v, dv)
  elif dv > 1e4 * abs(v):
    return '%.1g +- %.2g' % (v, dv)
  elif abs(v) >= 1e6 or abs(v) < 1e-7:
    exponent = np.floor(np.log10(abs(v)))
    fac = 10.**exponent
    mantissa = str(v/fac)
    exponent = "e" + ("%.0e" % fac).split("e")[-1]
    return mantissa + exponent

  # normal cases
  if dv >= 9.95:
    if abs(v) >= 9.5:
      return '%.0f(%.0f)' % (v, dv)
    else:
      ndecimal = ndec(abs(v), offset=1)
      return '%.*f(%.*f)' % (ndecimal, v, ndecimal, dv)
  if dv >= 0.995:
    if abs(v) >= 0.95:
      return '%.1f(%.1f)' % (v, dv)
    else:
      ndecimal = ndec(abs(v), offset=1)
      return '%.*f(%.*f)' % (ndecimal, v, ndecimal, dv,)
  else:
    ndecimal = max(ndec(abs(v), offset=1), ndec(dv) )
    return '%.*f(%.0f)' % (ndecimal, v, dv * 10. ** ndecimal)

def rec_dd():
    return defaultdict(rec_dd)

def set_scale(a,da):
    #using NIST data for hbar and c
    #hbar[eV] 6.582 119 514 x 10-16 eV s
    #speed of light [m/2] 299 792 458 m s-1
    #spacing must be in fermi
    hbarc=197.32697879518255
    scale=hbarc/a
    rel_err_scale=(da/a)
    return scale,rel_err_scale

def list_to_string(li):
  astr=''
  for i in range(len(li)):
    astr+=str(li[i])
  return astr

def string_to_list(smom):
  strlen = len(smom)
  tmplist=[]
  for i in range(strlen):
    tmplist.append(smom[i])
  
  tmplistlen=len(tmplist)
  if '-' in tmplist:
    negsign_loc = [minus_loc for minus_loc in range(tmplistlen) if '-'==tmplist[minus_loc]]
  
    for i in negsign_loc:
      tmp = int(tmplist[i]+tmplist[i+1])
      tmplist[i]=tmp

    droplocs=[i+1 for i in negsign_loc]
  
    reducedlist = np.delete(tmplist,droplocs).tolist()
    out=[]
    for i in range(len(reducedlist)):
      out.append(int(reducedlist[i]))
  else:
    out=[]
    for i in range(strlen):
      out.append(int(smom[i]))

  return out

def convert_mom_to_abs(str_mom):
  mom=string_to_list(str_mom)
  absmom=(mom[0]**2+mom[1]**2+mom[2]**2)
  out_str='.'+str(absmom)+'.'
  return out_str

def convert_mom_to_norm(str_mom):
  mom=string_to_list(str_mom)
  absmom=(mom[0]**2+mom[1]**2+mom[2]**2)
  if absmom==0:
    outstr='000'
  elif absmom==1:
    outstr='001'
  elif absmom==2:
    outstr='011'
  elif absmom==3:
    outstr='111'
  return outstr

def convert_pi_mom(ppi):
  listpi = string_to_list(ppi)
  abspi=int(np.dot(listpi,listpi))
  if abspi==1:
    out='001'
  elif abspi==2:
    out='011'
  elif abspi==3:
    out='111'
  elif abspi==0:
    out='000'
  return out


def AbsMomLatex(smom):
  lmom = string_to_list(smom)
  norm = lmom[0]**2+lmom[1]**2+lmom[2]**2
  snorm = "{0:1d}".format(norm)
  if snorm=='0':
    sqrtnorm = "$0$"
  elif snorm=='1':
    sqrtnorm = "$1$"
  else:
    sqrtnorm = "$\\sqrt{ "+str(snorm)+" }$"
  return sqrtnorm




class KP_coordinate:
  def __init__(self,p_pipi,p_f,q_c,irr,i_pol,c_pol,n,EV,EH,preVal):
    self.p_pipi = np.asarray(p_pipi)
    self.p_f = np.asarray(p_f)
    self.q_c = np.asarray(q_c)
    self.irr = irr
    self.i_pol = int(i_pol)
    self.c_pol = int(c_pol)
    self.n = int(n)
    self.EV = EV
    self.EH = EH
    self.preVal = preVal

