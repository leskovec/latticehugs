import matplotlib.pyplot as plt
import matplotlib as mpl
import itertools
import numpy as np
from matplotlib.texmanager import TexManager
from matplotlib.ticker import FormatStrFormatter


tableau30=["#000000", "#ffff00", "#1ce6ff", "#ff34ff", "#ff4a46", "#008941", "#006fa6", "#a30059",    "#ffdbe5", "#7a4900", "#0000a6", "#63ffac", "#b79762", "#004d43", "#8fb0ff", "#997d87",    "#5a0007", "#809693", "#feffe6", "#1b4400", "#4fc601", "#3b5dff", "#4a3b53", "#ff2f80",    "#61615a", "#ba0900", "#6b7900", "#00c2a0", "#ffaa92", "#ff90c9", "#b903aa", "#d16100",    "#ddefff", "#000035", "#7b4f4b", "#a1c299", "#300018", "#0aa6d8", "#013349", "#00846f",    "#372101", "#ffb500", "#c2ffed", "#a079bf", "#cc0744", "#c0b9b2", "#c2ff99", "#001e09",    "#00489c", "#6f0062", "#0cbd66", "#eec3ff", "#456d75", "#b77b68", "#7a87a1", "#788d66",    "#885578", "#fad09f", "#ff8a9a", "#d157a0", "#bec459", "#456648", "#0086ed", "#886f4c",    "#34362d", "#b4a8bd", "#00a6aa", "#452c2c", "#636375", "#a3c8c9", "#ff913f", "#938a81",    "#575329", "#00fecf", "#b05b6f", "#8cd0ff", "#3b9700", "#04f757", "#c8a1a1", "#1e6e00",    "#7900d7", "#a77500", "#6367a9", "#a05837", "#6b002c", "#772600", "#d790ff", "#9b9700",    "#549e79", "#fff69f", "#201625", "#72418f", "#bc23ff", "#99adc0", "#3a2465", "#922329",    "#5b4534", "#fde8dc", "#404e55", "#0089a3", "#cb7e98", "#a4e804", "#324e72", "#6a3a4c",    "#83ab58", "#001c1e", "#d1f7ce", "#004b28", "#c8d0f6", "#a3a489", "#806c66", "#222800",    "#bf5650", "#e83000", "#66796d", "#da007c", "#ff1a59", "#8adbb4", "#1e0200", "#5b4e51"]

cycol = itertools.cycle(tableau30)

def getkey_int(item,pl):
    return int(item[pl])
def getkey_str(item,pl):
    return item[pl]


def InitPlottingOneColumn(ratio):
    mpl.rcParams['figure.figsize'] = (3.9, 3.9/ratio)
    mpl.rcParams['savefig.dpi'] = 350

    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.unicode'] = True
    mpl.rcParams['font.size'] = 10

    mpl.rcParams['axes.labelsize'] = 10
    mpl.rcParams['axes.titlesize'] = 10
    mpl.rcParams['axes.linewidth'] = 1.4
    mpl.rcParams['axes.grid'] = True
    
    mpl.rcParams['legend.fontsize'] = 10
    mpl.rcParams['legend.frameon'] = True
    mpl.rcParams['legend.fancybox'] = True
    mpl.rcParams['legend.loc'] = 'upper right'
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['legend.scatterpoints'] = 1
    mpl.rcParams['legend.markerscale'] = 0.5
    mpl.rcParams['legend.labelspacing'] = 0.5
    mpl.rcParams['legend.borderaxespad'] = 0.0
    mpl.rcParams['legend.columnspacing'] = 0.3
    
    mpl.rcParams['xtick.labelsize'] = 10
    mpl.rcParams['ytick.labelsize'] = 10
    mpl.rcParams['xtick.major.size'] = 3
    mpl.rcParams['xtick.minor.size'] = 2
    mpl.rcParams['xtick.major.width'] = 1.25
    mpl.rcParams['xtick.minor.width'] = 1
    mpl.rcParams['ytick.major.size'] = 3
    mpl.rcParams['ytick.minor.size'] = 2
    mpl.rcParams['ytick.major.width'] = 1.25
    mpl.rcParams['ytick.minor.width'] = 1

    mpl.rcParams['grid.color'] = 'grey'
    mpl.rcParams['grid.alpha'] = 0.3
    mpl.rcParams['grid.linestyle'] = 'dashed'
    mpl.rcParams['grid.linewidth'] = 0.6
    
    mpl.rcParams['xtick.direction'] = 'in'
    mpl.rcParams['ytick.direction'] = 'in'
    mpl.rcParams['errorbar.capsize'] = 3 #default

def InitPlottingTwoColumn(ratio):
    mpl.rcParams['figure.figsize'] = (7.9, 7.9/ratio)
    mpl.rcParams['savefig.dpi'] = 350

    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.unicode'] = True
    mpl.rcParams['font.size'] = 10

    mpl.rcParams['axes.labelsize'] = 10
    mpl.rcParams['axes.titlesize'] = 10
    mpl.rcParams['axes.linewidth'] = 1.4
    mpl.rcParams['axes.grid'] = True
    
    mpl.rcParams['legend.fontsize'] = 10
    mpl.rcParams['legend.frameon'] = True
    mpl.rcParams['legend.fancybox'] = True
    mpl.rcParams['legend.loc'] = 'upper right'
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['legend.scatterpoints'] = 1
    mpl.rcParams['legend.markerscale'] = 0.5
    mpl.rcParams['legend.labelspacing'] = 0.5
    mpl.rcParams['legend.borderaxespad'] = 0.0
    mpl.rcParams['legend.columnspacing'] = 0.3
    
    mpl.rcParams['xtick.labelsize'] = 10
    mpl.rcParams['ytick.labelsize'] = 10
    mpl.rcParams['xtick.major.size'] = 3
    mpl.rcParams['xtick.minor.size'] = 2
    mpl.rcParams['xtick.major.width'] = 1.25
    mpl.rcParams['xtick.minor.width'] = 1
    mpl.rcParams['ytick.major.size'] = 3
    mpl.rcParams['ytick.minor.size'] = 2
    mpl.rcParams['ytick.major.width'] = 1.25
    mpl.rcParams['ytick.minor.width'] = 1

    mpl.rcParams['grid.color'] = 'grey'
    mpl.rcParams['grid.alpha'] = 0.3
    mpl.rcParams['grid.linestyle'] = 'dashed'
    mpl.rcParams['grid.linewidth'] = 0.6
    
    mpl.rcParams['xtick.direction'] = 'in'
    mpl.rcParams['ytick.direction'] = 'in'
    mpl.rcParams['errorbar.capsize'] = 2.5 #default

def InitPlotting3D(ratio):
    mpl.rcParams['figure.figsize'] = (5.5, 5.5/ratio)
    mpl.rcParams['savefig.dpi'] = 350

    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.unicode'] = True
    mpl.rcParams['font.size'] = 10

    mpl.rcParams['axes.labelsize'] = 10
    mpl.rcParams['axes.titlesize'] = 10
    mpl.rcParams['axes.linewidth'] = 1.4
    mpl.rcParams['axes.grid'] = True
    
    mpl.rcParams['legend.fontsize'] = 10
    mpl.rcParams['legend.frameon'] = True
    mpl.rcParams['legend.fancybox'] = True
    mpl.rcParams['legend.loc'] = 'upper right'
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['legend.scatterpoints'] = 1
    mpl.rcParams['legend.markerscale'] = 0.5
    mpl.rcParams['legend.labelspacing'] = 0.5
    mpl.rcParams['legend.borderaxespad'] = 0.0
    mpl.rcParams['legend.columnspacing'] = 0.3
    
    mpl.rcParams['xtick.labelsize'] = 10
    mpl.rcParams['ytick.labelsize'] = 10
    mpl.rcParams['xtick.major.size'] = 3
    mpl.rcParams['xtick.minor.size'] = 2
    mpl.rcParams['xtick.major.width'] = 1.25
    mpl.rcParams['xtick.minor.width'] = 1
    mpl.rcParams['ytick.major.size'] = 3
    mpl.rcParams['ytick.minor.size'] = 2
    mpl.rcParams['ytick.major.width'] = 1.25
    mpl.rcParams['ytick.minor.width'] = 1

    mpl.rcParams['grid.color'] = 'grey'
    mpl.rcParams['grid.alpha'] = 0.3
    mpl.rcParams['grid.linestyle'] = 'dashed'
    mpl.rcParams['grid.linewidth'] = 0.6
    
    mpl.rcParams['xtick.direction'] = 'in'
    mpl.rcParams['ytick.direction'] = 'in'
    mpl.rcParams['errorbar.capsize'] = 3 #default

# set global settings
def init_plotting(size_x,size_y):
    mpl.rcParams['figure.figsize'] = (size_x, size_y)
    mpl.rcParams['savefig.dpi'] = 350

    mpl.rcParams['text.usetex'] = True
    mpl.rcParams['text.latex.unicode'] = True
    mpl.rcParams['font.size'] = 17

    mpl.rcParams['axes.labelsize'] = 17
    mpl.rcParams['axes.titlesize'] = 19
    mpl.rcParams['axes.linewidth'] = 1.5
    mpl.rcParams['axes.grid'] = True

    #cant really mess with spines because the tick marks stay..
    #mpl.rcParams['axes.spines.right'] = True
    #mpl.rcParams['axes.spines.top'] = True
    
    mpl.rcParams['legend.fontsize'] = 17
    mpl.rcParams['legend.frameon'] = True
    mpl.rcParams['legend.fancybox'] = True
    mpl.rcParams['legend.loc'] = 'upper right'
    mpl.rcParams['legend.numpoints'] = 1
    mpl.rcParams['legend.scatterpoints'] = 1
    mpl.rcParams['legend.markerscale'] = 0.5
    mpl.rcParams['legend.labelspacing'] = 0.5
    mpl.rcParams['legend.borderaxespad'] = 0.0
    mpl.rcParams['legend.columnspacing'] = 0.3
    
    mpl.rcParams['xtick.labelsize'] = 17
    mpl.rcParams['ytick.labelsize'] = 17
    mpl.rcParams['xtick.major.size'] = 4
    mpl.rcParams['xtick.minor.size'] = 2
    mpl.rcParams['xtick.major.width'] = 1.3
    mpl.rcParams['xtick.minor.width'] = 1
    mpl.rcParams['ytick.major.size'] = 4
    mpl.rcParams['ytick.minor.size'] = 2
    mpl.rcParams['ytick.major.width'] = 1.3
    mpl.rcParams['ytick.minor.width'] = 1

    mpl.rcParams['grid.color'] = 'grey'
    mpl.rcParams['grid.alpha'] = 0.3
    mpl.rcParams['grid.linestyle'] = 'dashed'
    mpl.rcParams['grid.linewidth'] = 0.6
    
    #mpl.rcParams['xtick.direction'] = 'in'
    #mpl.rcParams['ytick.direction'] = 'in'
    mpl.rcParams['errorbar.capsize'] = 5 #default

def plot_effmass_nonint(quantity,corr,corr_sig,tmin,tmax,ymin,ymax,llist,nnlist):
        
    #define time axis
    time=np.linspace(tmin,tmax,tmax-tmin+1)

    # begin subplots region
    plt.subplot(111)
    plt.gca().margins(0.1, 0.1)
        
    #add plots
    for i in corr.copy():
        plt.errorbar(time,corr[i][tmin:tmax+1],c=cycol(),alpha=0.95, yerr=corr_sig[i][tmin:tmax+1],label=str(i))

    time2=np.linspace(tmin-4,tmax+4,num=2)
    for j in nnlist:
        yni=np.linspace(nnlist[j],nnlist[j],num=2)
        plt.plot(time2,yni,linewidth=1.5,color='black',label=str(j),linestyle='--')

    #set x limits
    plt.xlim([tmin-1,tmax+1])
    
    #tmp=list(itertools.chain(*corr.values()))
    #minx = min(x for x in tmp if x > 0)
    #maxx = max(x for x in tmp if x > 0)
    plt.ylim([ymin,ymax])

    plt.xlabel('$t \;[a]$')
    plt.ylabel('$m_{eff}^n(t)$')
    title=str(quantity)
    plt.title(title)
        
    plt.gca().legend(bbox_to_anchor = (1.0, 1.0))

    plt.show()
    
def plot_meff_list(quantity,fitted_a,fitted_s,tmin_fit,tmax_fit,corr,corr_sig,tmin,tmax,ymin,ymax,llist,nnlist,showfit):
        
    #define time axis
    time=np.linspace(tmin,tmax,tmax-tmin+1)

    # begin subplots region
    plt.subplot(111)
    plt.gca().margins(0.1, 0.1)
        
    colidx=0
    for i in llist:
        if i[0:4]=='meff':
            mlabel='$'+i[0]+'_{\mathrm{eff}}^{('+i[5:]+')}$'
        else:
            mlabel = i
        ccc=cycol()      
        ip=int(i[-1:])
        ttime=[int(jj) for jj in range(3,tmax_fit[ip]+1)]
        plt.errorbar(ttime,corr[i][ttime],c=ccc,alpha=1, yerr=corr_sig[i][ttime],label=mlabel,fmt='.')
        plt.plot(ttime,corr[i][ttime],c=ccc,alpha=0.2,linewidth=1)
        
        if showfit[i]=='yes':
            y_high=np.linspace(fitted_a[ip]+fitted_s[ip],fitted_a[ip]+fitted_s[ip], num=2)
            y_low=np.linspace(fitted_a[ip]-fitted_s[ip],fitted_a[ip]-fitted_s[ip], num=2)
            tt1=np.linspace(tmin_fit[ip],tmax_fit[ip],num=2)
            plt.fill_between(tt1,y_low,y_high,facecolor=ccc,alpha=0.7,linewidth=0.0)
        colidx+=2
    time2=np.linspace(tmin-2,tmax+2,num=2)

    for j in nnlist:
        yni=np.linspace(nnlist[j],nnlist[j],num=2)
        plt.plot(time2,yni,linewidth=1.5,color='black',label=str(j),linestyle='--')
        
    #set x limits
    plt.xlim([tmin-1,tmax+1])
    plt.ylim([ymin,ymax])

    plt.xlabel('$t \;[a]$')
    plt.ylabel('$m_{\mathrm{eff}}^{(n)}(t)$')
    title=str(quantity)
    plt.title(title)

    plt.gca().legend(bbox_to_anchor = (1.0, 1.0),ncol=3,borderpad=0.4)
    tmp=quantity.translate(None, '^,|+_=:!@#${}\ ')
    #print tmp
    plt.savefig(tmp+'.pdf',bbox_inches='tight')
    plt.show()
    