module fit_functions
    use cgr
    implicit none
    save
    real(8),allocatable,dimension(:)::loc_ev,loc_sigma
    real(8),allocatable,dimension(:,:)::loc_invcorr
    integer::tmax,tlower,tupper,iounit,ntsymm,nexp

    contains
        ! subroutine func_exp(n,p,f,g) is called by zxcgr
        ! subroutine fit_exp_n(ev,invcorr,tlowin,tmaxin,p,nx,io) does the fit
        ! function chi2_f_exp(ev,invcov,tlowin,tmaxin,p,nexp) gets chi2
        ! calls zxcgr with func_xxx
        
        subroutine fit_exp_n(ev,invcorr,tlowin,tmaxin,p,nx,sigma)
        
                ! fit to one exponential a exp(-mass t) 
                ! in the range t=1..tmaxin
                implicit none
        
                integer,intent(in)::tlowin,tmaxin,nx
                real(8),intent(in),dimension(tmaxin)::ev,sigma
                real(8),intent(in),dimension(tmaxin,tmaxin)::invcorr
                real(8),dimension(2*nx),intent(inout)::p
                real(8),dimension(:),allocatable::g,work,psave
                real(8)::chi2,dfpred,acc,chi2old
                integer::iter,ierr,npar,ti
                !external::func_exp
        
                nexp=nx
                npar=2*nexp
                tmax=tmaxin
                tlower=tlowin
                allocate(loc_ev(tmaxin),loc_sigma(tmaxin),loc_invcorr(tmaxin,tmaxin),g(npar),work(12*npar),psave(npar))
                loc_ev=ev
                loc_invcorr=invcorr
                loc_sigma=sigma
        
                ! sensible p or just zero?
                if(p(2).eq.0)then
                  p(1)=0.4d0
                  p(2)=0.5d0
                  if(nx.eq.2) then
                    p(3)=0.4d0
                    p(4)=0.8d0
                  end if
                end if
                psave=p
        
                call func_exp(npar,psave,chi2old,g)
                !print *,'A',p(1:2),chi2old
        
                ! estimate start value from lowest t points
                ti=(1+tmaxin)/2
                select case(nexp)
                case (1)
                  p(2)=-log(ev(tmaxin-1)/ev(1+1))/(tmaxin-1-2)
                  p(1)=ev(ti)*exp(p(2)*ti)
                case (2)
                  p(2)=-log(ev(tmaxin-1)/ev(1+1))/(tmaxin-1-2)
                  p(1)=ev(ti)*exp(p(2)*ti)
                  p(4)=p(2)+0.4d0
                  p(3)=p(1)+0.1d0
                case default
                  write(*,*) 'This case :',nexp,' is not implemented!'
                  stop
                end select
                call func_exp(npar,p,chi2,g)
                !print *,'B',p(1:2),chi2
                if(chi2.gt.chi2old)p=psave
        
                !if(nx.eq.1)  p(2)=log(p(2))
        
                !if(nx.eq.2)then
                !p(4)=log(p(4)-p(2))
                !p(2)=log(p(2))
                !end if
        
                dfpred=0.001d0   !may adjust to size of largest sigma?
                iter=2000
                acc=1.d-8
        
                call zxcgr(func_exp,npar,acc,iter,dfpred,p,g,chi2,work,ierr,1333)          
                if(ierr.ne.0)then
                write(132,'(a,e15.7,4i5)')'zxcgr did not converge ',chi2,ierr,1,tmaxin
                else 
                write(132,'(a,e15.7)')    'zxcgr did converge to  ',chi2
                end if
                !print *,'C',p(1:2),chi2
        
                deallocate(loc_ev,loc_invcorr,loc_sigma,g,work)
                !select case(nexp)
                !case (1)
                !p(2)=p(2)
                !case (2)
                !p(4)=exp(p(2))+exp(p(4))
                !p(2)=exp(p(2))
                !case default
                !  write(*,*) 'This case :',nexp,' is not implemented!'
                !  stop
                !end select
        
        end subroutine fit_exp_n
        
        subroutine func_exp(n,pin,f,g)
                ! p(i) for n=4 are in log convention "external"

                implicit none
                
                integer,intent(in)::n
                real(8),intent(in),dimension(n)::pin
                real(8),intent(out),dimension(n)::g
                real(8),intent(out)::f
                real(8),dimension(4)::p
                integer::t,ts,ndf
                
                if(pin(1).gt.1.d20)then
                print *,'SR func_exp emergecy: input parameter',p
                end if
                f=0
                select case(nexp)
                
                case (1)
                p(1)=pin(1)
                p(2)=pin(2)
                do t=1,tmax
                do ts=1,tmax
                  f=f + (p(1)*exp(-p(2)*t)-loc_ev(t))*&
                  &     (p(1)*exp(-p(2)*ts)-loc_ev(ts))*&
                  &     loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                end do
                end do
                ndf=tmax-2
                case (2)
                p=pin

                do t=1,tmax
                do ts=1,tmax
                  f=f + (p(1)*exp(-p(2)*t) +p(3)*exp(-p(4)*t)-loc_ev(t))*&
                  &     (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
                  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                end do
                end do
                ndf=tmax-4
                case default
                  print *,'SR func_exp: case not implemented!'
                  stop
                end select
                f=f/ndf
                if(f.gt.1.d15)then
                  write(133,'(a)')'SR func_exp emergency: result reset because too large:',f
                  f=1.d15+20.d0*dlog(f)
                end if
                
                g=0
                
                select case (nexp)
                
                case (1)
                do t=1,tmax
                do ts=1,tmax
                g(1)=g(1)       +(  exp(-p(2)*t)*(p(1)*exp(-p(2)*ts)-loc_ev(ts))&
                &                               +(p(1)*exp(-p(2)*t) -loc_ev(t))*exp(-p(2)*ts)  )*&
                &                   loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                
                g(2)=g(2)       +( -t*p(1)*exp(-p(2)*t)*(p(1)*exp(-p(2)*ts)-loc_ev(ts))&
                &                                      -(p(1)*exp(-p(2)*t) -loc_ev(t))*ts*p(1)*exp(-p(2)*ts)  )*&
                &                   loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                end do
                end do
                ndf=tmax-2
                !write(*,*)'F',p(1:2),f,g(1:2)
                case (2)
                do t=1,tmax
                do ts=1,tmax
                g(1)=g(1)+& 
                &         exp(-p(2)*t)*&
                &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
                &        +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
                &         exp(-p(2)*ts)*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                g(3)=g(3)+& 
                &          exp(-p(4)*t)*&
                &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
                &        +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
                &         exp(-p(4)*ts)*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                
                g(2)=g(2)+&
                &         (p(1)*exp(-p(2)*t)*(-t))*&
                &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
                &         +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
                &         (p(1)*exp(-p(2)*ts)*(-ts))*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                
                g(4)=g(4)+&
                &         (p(3)*exp(-p(4)*t)*(-t))*&
                &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
                &         +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
                &         (p(3)*exp(-p(4)*ts)*(-ts))*&
                &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                !g(2)=g(2)*exp(pin(2))+g(4)*exp(pin(2))
                !g(4)=g(4)*exp(pin(4))
                
                end do
                end do
                ndf=tmax-4
                end select
                g=g/ndf     
        end subroutine func_exp
        
        function chi2_f_exp(ev,invcov,tlowin,tmaxin,p,nexp,sigma)
                
                ! Here p(i) are defined like in the funtion
                ! p(1)*exp(-p(2)*t) + p(3)* exp(-p(4)*t)
                
                implicit none
                
                integer,intent(in)::tlowin,tmaxin,nexp
                real(8),intent(in),dimension(2*nexp)::p
                real(8),intent(in),dimension(tmaxin)::ev,sigma
                real(8),intent(in),dimension(tmaxin,tmaxin)::invcov
                real(8) ::chi2_f_exp
                integer::t,ts,ndf
                real(8)::sum
                
                sum=0
                select case(nexp)
                
                case (1)
                
                do t=1,tmaxin
                do ts=1,tmaxin
                sum=sum + (p(1)*exp(-p(2)*t)-ev(t))*&
                &         (p(1)*exp(-p(2)*ts)-ev(ts))*&
                &         invcov(t,ts)/(sigma(t)*sigma(ts))
                end do
                end do
                ndf=tmaxin-2
                case (2)
                do t=1,tmaxin
                do ts=1,tmaxin
                sum=sum + (p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-ev(t))*&
                &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-ev(ts))*&
                &         invcov(t,ts)/(sigma(t)*sigma(ts))
                end do
                end do
                ndf=tmaxin-4
                
                end select
                chi2_f_exp=sum/ndf
        end function chi2_f_exp

        subroutine fit_const(ev,invcorr,tlowin,tmaxin,p,nx,sigma)
        
                ! fit to one exponential a exp(-mass t) 
                ! in the range t=1..tmaxin
                implicit none
        
                integer,intent(in)::tlowin,tmaxin,nx
                real(8),intent(in),dimension(tmaxin)::ev,sigma
                real(8),intent(in),dimension(tmaxin,tmaxin)::invcorr
                real(8),dimension(2*nx),intent(inout)::p
                real(8),dimension(:),allocatable::g,work,psave
                real(8)::chi2,dfpred,acc,chi2old
                integer::iter,ierr,npar,ti
                !external::func_exp
        
                nexp=nx
                npar=1
                tmax=tmaxin
                tlower=tlowin
                allocate(loc_ev(tmaxin),loc_sigma(tmaxin),loc_invcorr(tmaxin,tmaxin),g(npar),work(12*npar),psave(npar))
                loc_ev=ev
                loc_invcorr=invcorr
                loc_sigma=sigma
        
                ! sensible p or just zero?
                if(p(1).eq.0)then
                  p(1)=ev(1)
                end if
                psave=p
        
                call func_const(npar,psave,chi2old,g)
        
                ! estimate start value from lowest t points
                ti=(1+tmaxin)/2
                p(1)=ev(ti)

                call func_const(npar,p,chi2,g)
                !print *,'B',p(1:2),chi2
                if(chi2.gt.chi2old)p=psave
        
                dfpred=0.001d0   !may adjust to size of largest sigma?
                iter=2000
                acc=1.d-8
        
                call zxcgr(func_const,npar,acc,iter,dfpred,p,g,chi2,work,ierr,1333)          
                if(ierr.ne.0)then
                write(132,'(a,e15.7,4i5)')'zxcgr did not converge ',chi2,ierr,1,tmaxin
                else 
                write(132,'(a,e15.7)')    'zxcgr did converge to  ',chi2
                end if
                !print *,'C',p(1:2),chi2
        
                deallocate(loc_ev,loc_invcorr,loc_sigma,g,work)
                !select case(nexp)
                !case (1)
                !p(2)=p(2)
                !case (2)
                !p(4)=exp(p(2))+exp(p(4))
                !p(2)=exp(p(2))
                !case default
                !  write(*,*) 'This case :',nexp,' is not implemented!'
                !  stop
                !end select
        
        end subroutine fit_const

        subroutine func_const(n,pin,f,g)
                ! p(i) for n=4 are in log convention "external"

                implicit none
                
                integer,intent(in)::n
                real(8),intent(in),dimension(1)::pin
                real(8),intent(out),dimension(1)::g
                real(8),intent(out)::f
                real(8),dimension(1)::p
                integer::t,ts,ndf
                
                if(pin(1).gt.1.d20)then
                print *,'SR func_exp emergecy: input parameter',p
                end if
                f=0

                p(1)=pin(1)
                do t=1,tmax
                do ts=1,tmax
                  f=f + (p(1)-loc_ev(t))*&
                  &     (p(1)-loc_ev(ts))*&
                  &     loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                end do
                end do
                ndf=tmax-1

                f=f/ndf
                
                g=0
                
                do t=1,tmax
                do ts=1,tmax
                g(1)=g(1)       +(  (p(1)-loc_ev(ts))&
                &                  +(p(1) -loc_ev(t))  )*&
                &                   loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
                end do
                end do
                ndf=tmax-1
                g=g/ndf     
        end subroutine func_const

        function chi2_f_const(ev,invcov,tlowin,tmaxin,p,nexp,sigma)
                
                ! p(1)

                implicit none
                
                integer,intent(in)::tlowin,tmaxin,nexp
                real(8),intent(in),dimension(1)::p
                real(8),intent(in),dimension(tmaxin)::ev,sigma
                real(8),intent(in),dimension(tmaxin,tmaxin)::invcov
                real(8) ::chi2_f_const
                integer::t,ts,ndf
                real(8)::sum
                
                sum=0
            
                do t=1,tmaxin
                do ts=1,tmaxin
                sum=sum + (p(1)-ev(t))*&
                &         (p(1)-ev(ts))*&
                &         invcov(t,ts)/(sigma(t)*sigma(ts))
                end do
                end do
                ndf=tmaxin-1
                chi2_f_const=sum/ndf
        end function chi2_f_const

end module fit_functions