module fitting_2exp

  implicit none
  contains

  !########################################!
  subroutine do_exp_fit_2exp(result_file,eigval,eigval_avg,eigval_jack,nkonf,nt,tzero,t1,t2,svdcut,pstart,result_2expC,result_2expU,chi2)
    !eigval and eigval avg are used calculate the correlation matrix, eigval_jack is used to fit
    use fit_functions
    use inversion
    use cgr
    implicit none
  
    integer,intent(in)::nt,nkonf,tzero
    integer,dimension(2),intent(in)::t1,t2
    real(8),dimension(nkonf,nt),intent(in)::eigval
    real(8),dimension(nt),intent(in)::eigval_avg
    real(8),dimension(nt),intent(in)::eigval_jack
    character(len=*), intent(in) :: result_file
    real(8), dimension(4), intent(out) :: result_2expC,result_2expU
    real(8), intent(out) :: chi2
    real(8),dimension(:,:),allocatable::eigval_correl
    real(8),dimension(:,:),allocatable:: correl,covar_save,ic
    real(8),dimension(:,:),allocatable:: correla
    real(8),dimension(:),allocatable:: ev,eval,eveps,evx,sigma
    real(8),dimension(4),intent(in)::pstart
    real(8),dimension(4)::p,peps,pa,px,psave
    real(8),intent(in)::svdcut
    real(8)::correldet,chi2a,corrfac
    integer::ifail,i,j,n,t,t0,tlow,thigh,tdel,nop,igraph,ngraph
    integer,dimension(:),allocatable::lvec,mvec
    character(len=256)::fn
    real(8),dimension(:),allocatable :: work
    real(8),dimension(1) :: testwork
    integer::info,lwork,ne,ndf
    logical::svd,corrflag
    character(len=1)::token
    integer :: efit_fileunit
  
    allocate(eval(nt),eveps(nt),lvec(nt),mvec(nt),eigval_correl(nt,nt))
    efit_fileunit=28
      open(unit=efit_fileunit,file=trim(result_file),status="unknown",form="formatted")

    !initialize
    p=pstart
    psave=p

    eval(:)=eigval_avg(:)
    
    call mk_eigval_correl(eigval,eval,eigval_correl,nt,nkonf)

    t0=tzero
  
    do t=1,nt
      eveps(t)=sqrt(eigval_correl(t,t))
    end do
  
    do tlow=t1(1),t1(2)
      do thigh=t2(1),t2(2)
        corrflag=.true.
        tdel=thigh+1-tlow

        !if(tdel.lt.3)then
        !  print *,'Combination ',tlow,thigh,' skipped: to few points'
        !  goto 1
        !end if
  
        allocate(correl(tdel,tdel),covar_save(tdel,tdel),ic(tdel,tdel),ev(tdel),sigma(tdel))
    
        sigma(1:tdel)=eveps(tlow:tlow-1+tdel)
        !use normalized correlation matrix, correct chi2 later
        do i=1,tdel
          do j=1,tdel
            correl(i,j)=eigval_correl(tlow-1+i,tlow-1+j)/(sigma(i)*sigma(j))
          end do!j
        end do!i
  
        covar_save=correl
  
        do i=1,tdel
          ev(i)=eigval_jack(tlow-1+i)
        end do!i
  
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        !analyze cov matrix for stability and positivity
        !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
        allocate(correla(tdel,tdel),evx(tdel))
        correla=correl
        lwork=2*nt-1
        !real symm matrix: better use DSYEV
        !call ZHEEV( 'V','U',tdel, correla, tdel, evx, testwork,-1,rwork,info)
        call DSYEV( 'V','U',tdel, correla, tdel, evx, testwork,-1,info)
        if(info.eq.0)then
          lwork=testwork(1)+1
        else
          print *,'do_exp_fit: ZEEHV returned with info =',info
        end if
        allocate(work(lwork))
        correla=correl
        !call ZHEEV( 'V','U',tdel, correla, tdel, evx, work,lwork,rwork,info)
        call DSYEV( 'V','U',tdel, correla, tdel, evx, work,lwork,info)
        !deallocate(work,rwork)
        deallocate(work)
    
        if(info.ne.0)then
          write(132,*)'ERROR in do_exp_fit for correl diagonalization with info =', info
          stop
        end if
    
        ! the svd criterion should be better : exp decay possible and ok
        ! if too fast take mean of lowest eigenvalues
  
        svd=.false.
        !svdslope=log(maxval(abs(evx))/minval(abs(evx)))/(tdel-1)
  
        if((tdel.gt.8).and.(any(evx.le.0).or.(minval(abs(evx)).lt.svdcut)))then   
          write(132,*)'do_exp_fit: covariance matrix badly tuned for tlow, thigh=',tlow,thigh
          write(132,'(a,/,5(8e12.3))')' lambda(correl) =',evx(1:tdel)
          write(132,*)'svd approximation, eigenvalues retuned'
          svd=.true.
        end if
  
        if(svd)then
  
          covar_save=0
          ndf=tdel     !counts the addition d.f. removed
          do n=1,tdel
            ! svd:
            ! add the projector to inverse
            if(evx(n).gt.svdcut)then
              ndf=ndf-1
              do i=1,tdel
                do j=1,tdel
                  covar_save(i,j)=covar_save(i,j)+correla(i,n)*correla(j,n)/evx(n)
                end do
              end do
            end if
          end do
  
        else
  
          ndf=0
          covar_save=correl
          call invert (covar_save,tdel,correldet,ifail,lvec,mvec,1.d-8)  
          if(ifail.ne.0) then
            write(132,*)'inversion failed for nop =',nop,' and det= ',correldet
            write(132,*)'used diagonal cov (uncorrelated fit) instead'
            corrflag=.false.
            goto 5
          end if
  5 continue
        end if
  
        deallocate(correla)
        
  
        !!!!!!!!!!!!!!!!!!!!!!
        ! 2 exp fits
        !!!!!!!!!!!!!!!!!!!!!!
  
        if(tdel.lt.5)then
          print *,'Combination ',tlow,thigh,' skipped for 2 exp fit'
          goto 2
        end if

        corrflag=.false.
        ne=2
        !!!!!!!!!!!!!!!!!!!!!!
        ! 2 exp fit uncorrelated
        !!!!!!!!!!!!!!!!!!!!!!
  
        ic(:,:)=0
        do t=1,tdel
          ic(t,t)=1.d0/correl(t,t)
        end do
  
        p=psave
        call fit_exp_n(ev,ic,tlow,tdel,p,ne,sigma)
        if(p(4).lt.p(2))then
          pa(1:2)=p(3:4)
          p(3:4)=p(1:2)
          p(1:2)=pa(1:2)
        end if
        chi2=chi2_f_exp(ev,ic,tlow,tdel,p,ne,sigma)
        px=pstart
        call fit_exp_n(ev,ic,tlow,tdel,px,ne,sigma)
        if(px(4).lt.px(2))then
          pa(1:2)=px(3:4)
          px(3:4)=px(1:2)
          px(1:2)=pa(1:2)
        end if
        chi2a=chi2_f_exp(ev,ic,tlow,tdel,px,ne,sigma)
        if(chi2a.lt.chi2)then
          p=px
          chi2=chi2a
        end if
  
        psave=p
  
        token='.'
        if(chi2.gt.3.0)token='*'
          write(efit_fileunit,'(1x,a1,1x,a1,4i5,3x,e14.7,4(5x,e16.8,3x,e16.8))')token,'u',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
        result_2expU=p
        p=psave
    
        corrflag=.true.
        if(corrflag)then
      
          !!!!!!!!!!!!!!!!!!!!!!
          ! 2 exp fit correlated
          !!!!!!!!!!!!!!!!!!!!!!
      
          ic=covar_save
      
          p=psave
          call fit_exp_n(ev,ic,tlow,tdel,p,ne,sigma)
          if(p(4).lt.p(2))then
            pa(1:2)=p(3:4)
            p(3:4)=p(1:2)
            p(1:2)=pa(1:2)
          end if
          chi2=chi2_f_exp(ev,ic,tlow,tdel,p,ne,sigma)
          px=pstart
          call fit_exp_n(ev,ic,tlow,tdel,px,ne,sigma)
          if(px(4).lt.px(2))then
            pa(1:2)=px(3:4)
            px(3:4)=px(1:2)
            px(1:2)=pa(1:2)
          end if
          chi2a=chi2_f_exp(ev,ic,tlow,tdel,px,ne,sigma)
          if(chi2a.lt.chi2)then
            p=px
            chi2=chi2a
          end if
          psave=p
          corrfac=1.
          if(tdel-4-ndf.gt.1)corrfac=real(tdel-4)/real(tdel-4-ndf)
          chi2=chi2*corrfac
      
          token='.'
          if(chi2.gt.3.0)token='*'
          if(svd)token='S'
          write(efit_fileunit,'(1x,a1,1x,a1,4i5,3x,e14.7,4(5x,e16.8,3x,e16.8))')token,'c',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
          !write(*,'(1x,a1,1x,a1,4i5,3x,e14.7,4(5x,e16.8,3x,e16.8))')token,'c',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
          write(*,'(1x,a1,1x,a1,4i5,3x,f14.7,4(5x,f16.8,3x,f16.8))')token,'c',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
          result_2expC=p
          p=psave
        end if
      
2 continue
  
        deallocate(correl,covar_save,ic,ev,evx,sigma)
  
1 continue
  
  
      end do !tdown
    end do !thigh
  
  
    deallocate(lvec,mvec,eval,eveps)
  end subroutine do_exp_fit_2exp
  
  
  subroutine mk_eigval_correl(eigval,eigval_avg,eigval_correl,nt,nkonf)
  ! compute covariance matrix using the overal mean
  ! This is Cov^(-1); cf. Michael, PRD49(1994)2616
  implicit none
  
  integer,intent(in)::nt,nkonf
  real(8),dimension(nkonf,nt),intent(in)::eigval
  real(8),dimension(nt),intent(in)::eigval_avg
  real(8),dimension(nt,nt),intent(out)::eigval_correl
  integer::i,t1,t2,k
  
  eigval_correl=0.0d0
  
  do t1=1,nt
    do t2=1,nt
      do k=1,nkonf
        eigval_correl(t1,t2)=eigval_correl(t1,t2)+(eigval(k,t1)-eigval_avg(t1))*(eigval(k,t2)-eigval_avg(t2))
      end do
    end do
  end do

  eigval_correl=eigval_correl*dble(nkonf-1)/dble(nkonf)
  
  end subroutine mk_eigval_correl
end module fitting_2exp