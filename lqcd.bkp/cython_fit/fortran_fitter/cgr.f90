module cgr

	contains                                      
		subroutine zxcgr  (funct,n,acc,maxfn,dfpred,x,g,f,w,ier,iounit)          
		!                                  specifications for arguments         
		!   purpose             - a conjugate gradient algorithm for finding
		!                           the minimum of a function of n variables
		!
		!   usage               - call zxcgr (funct,n,acc,maxfn,dfpred,x,g,f,w,
		!                           ier)
		!
		!   arguments    funct  - a user supplied subroutine which calculates
		!                           the objective function and its gradient
		!                           for given parameter values
		!                           x(1),x(2),...,x(n).
		!                           the calling sequence has the following form
		!                           call funct (n,x,f,g)
		!                           where x and g are vectors of length n.
		!                           the scalar f is for the objective function.
		!                           g(1), g(2), ..., g(n) are for the components
		!                           of the gradient of f.
		!                           funct must appear in an external statement
		!                           in the calling program. funct must not
		!                           alter the values of x(i),i=1,...,n or n.
		!                n      - the number of parameters of the objective
		!                           function. (input) (i.e.,the length of x)
		!                acc    - convergence criterion. (input)
		!                           the calculation ends when the sum of squares
		!                           of the components of g is less than acc.
		!                maxfn  - maximum number of function evaluations (i.e.,
		!                           calls to subroutine funct) allowed. (input)
		!                           if maxfn is set to zero, then there is
		!                           no restriction on the number of function
		!                           evaluations.
		!                dfpred - a rough estimate of the expected reduction
		!                           in f, which is used to determine the size
		!                           of the initial change to x. (input)
		!                           note that dfpred is the expected reduction
		!                           itself, and does not depend on any ratios.
		!                           a bad value of dfpred causes an error
		!                           message, with ier=129, and a return on the
		!                           first iteration. (see the description of
		!                           ier below)
		!                x      - vector of length n containing parameter
		!                           values.
		!                         on input, x must contain the initial
		!                           parameter estimates.
		!                         on output, x contains the final parameter
		!                           estimates as determined by zxcgr.
		!                g      - a vector of length n containing the
		!                           components of the gradient of f at the
		!                           final parameter estimates. (output)
		!                f      - a scalar containing the value of the function
		!                           at the final parameter estimates. (output)
		!                w      - work vector of length 6*n.
		!                ier    - error parameter. (output)
		!                         ier = 0 implies that convergence was
		!                           achieved and no errors occurred.
		!                         terminal error
		!                           ier = 129 implies that the line search of
		!                             an integration was abandoned. this
		!                             error may be caused by an error in the
		!                             gradient.
		!                           ier = 130 implies that the calculation
		!                             cannot continue because the search
		!                             direction is uphill.
		!                           ier = 131 implies that the iteration was
		!                             terminated because maxfn was exceeded.
		!                           ier = 132 implies that the calculation
		!                             was terminated because two consecutive
		!                             iterations failed to reduce f.
		!
		!   precision/hardware  - single and double/h32
		!                       - single/h36,h48,h60
		!
		!   reqd. imsl routines - uertst,ugetio
		!
		!   notation            - information on special notation and
		!                           conventions is available in the manual
		!                           introduction or through imsl routine uhelp
		!
		!   remarks  1.  the routine includes no thorough checks on the part
		!                of the user program that calculates the derivatives
		!                of the objective function. therefore, because
		!                derivative calculation is a frequent source of
		!                error, the user should verify independently the
		!                correctness of the derivatives that are given to
		!                the routine.
		!            2.  because of the close relation between the conjugate
		!                gradient method and the method of steepest descents,
		!                it is very helpful to choose the scale of the
		!                variables in a way that balances the magnitudes of
		!                the components of a typical derivate vector. it
		!                can be particularly inefficient if a few components
		!                of the gradient are much larger than the rest.
		!            3.  if the value of the parameter acc in the argument
		!                list of the routine is set to zero, then the
		!                subroutine will continue its calculation until it
		!                stops reducing the objective function. in this case
		!                the usual behaviour is that changes in the
		!                objective function become dominated by computer
		!                rounding errors before precision is lost in the
		!                gradient vector. therefore, because the point of
		!                view has been taken that the user requires the
		!                least possible value of the function, a value of
		!                the objective function that is small due to
		!                computer rounding errors can prevent further
		!                progress. hence the precision in the final values
		!                of the variables may be only about half the
		!                number of significant digits in the computer
		!                arithmetic, but the least value of f is usually
		!                found to quite high accuracy.
		!                                                                       
		!   copyright           - 1978 by imsl, inc. all rights reserved.       
		!                                                                       
		!   warranty            - imsl warrants only that imsl testing has been 
		!                           applied to this code. no other warranty,    
		!                           expressed or implied, is applicable.        
		                                                                       
		!      implicit double precision (a-h,o-z)
		      implicit none
		      
		      external funct
		      integer            n,maxfn,ier                                    
		      real*8             acc,dfpred,x(n),g(n),f,w(1)                    
		!                                  specifications for local variables   
		      integer            maxlin,mxfcon,i,iginit,igopt,iretry,irsdg,  &   
		     &                   irsdx,iterc,iterfm,iterrs,ixopt,ncalls,nfbeg, & 
		     &                   nfopt,iounit                                        
		      real*8             beta,ddspln,dfpr,fch,finit,fmin,gamden,gama,   &
		     &                   ginit,gmin,gnew,gspln,gsqrd,sbound,step,stepch,&
		     &                   stmin,sum,work                                 
		      data               maxlin/5/,mxfcon/2/                            
		!                                  first executable statement           
		      ier = 0                                                           
		      irsdx = n                                                         
		      irsdg = irsdx+n                                                   
		      iginit = irsdg+n                                                  
		      ixopt = iginit+n                                                  
		      igopt = ixopt+n                                                   
		      iterc = 0                                                         
		      ncalls = 0                                                        
		      iterfm = iterc                                                    
		    5 ncalls = ncalls+1                                                 
		!      print *,'cgr ',x(1:n)
		      call funct (n,x,f,g)                                              
		      if (ncalls.ge.2) go to 20                                         
		   10 do   i=1,n                                                       
		        w(i) = -g(i)
		      enddo                                                      
		      iterrs = 0                                                        
		      if (iterc.gt.0) go to 80                                          
		   20 gnew = 0.0                                                        
		      sum = 0.0                                                         
		      do   i=1,n                                                       
		         gnew = gnew+w(i)*g(i)                                          
		        sum = sum+g(i)**2      
		      enddo                                           
		      if (ncalls.eq.1) go to 35                                         
		      fch = f-fmin                                                      
		      if (fch) 35,30,50                                                 
		   30 if (gnew/gmin.lt.-1.0) go to 45                                   
		   35 fmin = f                                                          
		      gsqrd = sum                                                       
		      nfopt = ncalls                                                    
		      do  i=1,n                                                       
		        w(ixopt+i) = x(i)                                              
		        w(igopt+i) = g(i)                                                 
		      enddo                                           
		   45 if (sum.le.acc) go to 9005                                        
		   50 if (ncalls.ne.maxfn) go to 55                                     
		      ier = 131                                                         
		      go to 9000                                                        
		   55 if (ncalls.gt.1) go to 100                                        
		      dfpr = dfpred                                                     
		      stmin = dfpred/gsqrd                                              
		   80 iterc = iterc+1                                                   
		      finit = f                                                         
		      ginit = 0.0                                                       
		      do  i=1,n                                                       
		        w(iginit+i) = g(i)                                             
		        ginit = ginit+w(i)*g(i)                                           
		      enddo                                           
		      if (ginit.ge.0.0) go to 165                                       
		      gmin = ginit                                                      
		      sbound = -1.0                                                     
		      nfbeg = ncalls                                                    
		      iretry = -1                                                       
		      stepch = dmin1(stmin,abs(dfpr/ginit))                             
		      stmin = 0.0                                                       
		   90 step = stmin+stepch                                               
		      work = 0.0                                                        
		      do  i=1,n                                                       
		        x(i) = w(ixopt+i)+stepch*w(i)                                  
		        work = dmax1(work,abs(x(i)-w(ixopt+i)))                           
		      enddo                                           
		      if (work.gt.0.0) go to 5                                          
		      if (ncalls.gt.nfbeg+1) go to 115                                  
		      if (abs(gmin/ginit)-0.2) 170,170,115                              
		  100 work = (fch+fch)/stepch-gnew-gmin                                 
		      ddspln = (gnew-gmin)/stepch                                       
		      if (ncalls.gt.nfopt) sbound = step                                
		      if (ncalls.gt.nfopt) go to 105                                    
		      if (gmin*gnew.le.0.0) sbound = stmin                              
		      stmin = step                                                      
		      gmin = gnew                                                       
		      stepch = -stepch                                                  
		  105 if (fch.ne.0.0) ddspln = ddspln+(work+work)/stepch                
		      if (gmin.eq.0.0) go to 170                                        
		      if (ncalls.le.nfbeg+1) go to 120                                  
		      if (abs(gmin/ginit).le.0.2) go to 170                             
		  110 if (ncalls.lt.nfopt+maxlin) go to 120                             
		  115 ier = 129                                                         
		      go to 170                                                         
		  120 stepch = 0.5*(sbound-stmin)                                       
		      if (sbound.lt.-0.5) stepch = 9.0*stmin                            
		      gspln = gmin+stepch*ddspln                                        
		      if (gmin*gspln.lt.0.0) stepch = stepch*gmin/(gmin-gspln)          
		      go to 90                                                          
		  125 sum = 0.0                                                         
		      do  i=1,n                                                      
		        sum = sum+g(i)*w(iginit+i)  
		      enddo                                      
		      beta = (gsqrd-sum)/(gmin-ginit)                                   
		      if (abs(beta*gmin).le.0.2*gsqrd) go to 135                        
		      iretry = iretry+1                                                 
		      if (iretry.le.0) go to 110                                        
		  135 if (f.lt.finit) iterfm = iterc                                    
		      if (iterc.lt.iterfm+mxfcon) go to 140                             
		      ier = 132                                                         
		      go to 9000                                                        
		  140 dfpr = stmin*ginit                                                
		      if (iretry.gt.0) go to 10                                         
		      if (iterrs.eq.0) go to 155                                        
		      if (iterc-iterrs.ge.n) go to 155                                  
		      if (abs(sum).ge.0.2*gsqrd) go to 155                              
		      gama = 0.0                                                        
		      sum = 0.0                                                         
		      do  i=1,n                                                      
		        gama = gama+g(i)*w(irsdg+i)                                    
		        sum = sum+g(i)*w(irsdx+i)
		      enddo                                        
		      gama = gama/gamden                                                
		      if (abs(beta*gmin+gama*sum).ge.0.2*gsqrd) go to 155               
		      do  i=1,n                                                      
		        w(i) = -g(i)+beta*w(i)+gama*w(irsdx+i)  
		      enddo                          
		      go to 80                                                          
		  155 gamden = gmin-ginit                                               
		      do  i=1,n                                                      
		        w(irsdx+i) = w(i)                                              
		        w(irsdg+i) = g(i)-w(iginit+i)                                  
		        w(i) = -g(i)+beta*w(i)
		      enddo                                            
		      iterrs = iterc                                                    
		      go to 80                                                          
		  165 ier = 130                                                         
		  170 if (ncalls.eq.nfopt) go to 180                                    
		      f = fmin                                                          
		      do i=1,n                                                      
		        x(i) = w(ixopt+i)                                              
		        g(i) = w(igopt+i)
		      enddo                                            
		  180 if (ier.eq.0) go to 125                                           
		 9000 continue                                                          
		
		      call uertst (ier,'zxcgr ',iounit)
		 9005 return                                                            
		      end                                                               
                                      
		      subroutine uertst (ier,name,iounit)                                      
		      implicit double precision (a-h,o-z)
		      integer            ier                                            
		      character (len=6)  ::name,namset,nameq                                 
		      integer            ieq,ieqdf,iounit,level,levold                   
		      data               namset/'uerset'/                
		      data               nameq /'      '/                                   
		      data               level/4/,ieqdf/0/,ieq/1h=/                     
		
		      if (ier.gt.999) go to 25                                          
		      if (ier.lt.-32) go to 55                                          
		      if (ier.le.128) go to 5                                           
		      if (level.lt.1) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,35) ier,nameq,ieq,name            
		      if (ieqdf.eq.0) write(iounit,35) ier,name                       
		      go to 30                                                          
		    5 if (ier.le.64) go to 10                                           
		      if (level.lt.2) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,40) ier,nameq,ieq,name             
		      if (ieqdf.eq.0) write(iounit,40) ier,name                      
		      go to 30                                                          
		   10 if (ier.le.32) go to 15                                           
		      if (level.lt.3) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,45) ier,nameq,ieq,name            
		      if (ieqdf.eq.0) write(iounit,45) ier,name                      
		      go to 30                                                          
		   15 continue                                                          
		      if (name.ne.namset) go to 25                           
		      levold = level                                                    
		      level = ier                                                       
		      ier = levold                                                      
		      if (level.lt.0) level = 4                                         
		      if (level.gt.4) level = 4                                         
		      go to 30                                                          
		   25 continue                                                          
		      if (level.lt.4) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,50) ier,nameq,ieq,name            
		      if (ieqdf.eq.0) write(iounit,50) ier,name                       
		   30 ieqdf = 0                                                         
		      return                                                            
		   35 format(19h *** terminal error,10x,7h(ier = ,i3,   &                
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   40 format(27h *** warning with fix error,2x,7h(ier = ,i3,  &          
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   45 format(18h *** warning error,11x,7h(ier = ,i3,       &             
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   50 format(20h *** undefined error,9x,7h(ier = ,i5,     &              
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   55 ieqdf = 1                                                         
		      nameq= name
		   65 return                                                            
		      end                                                               
                                  
		      subroutine ugetio(iopt,nin,nout)                                  
		      implicit double precision (a-h,o-z)
		      integer            iopt,nin,nout                                  
		      integer            nind,noutd                                     
		      data               nind/5/,noutd/6/                               
		      if (iopt.eq.3) go to 10                                           
		      if (iopt.eq.2) go to 5                                            
		      if (iopt.ne.1) go to 9005                                         
		      nin = nind                                                        
		      nout = noutd                                                      
		      go to 9005                                                        
		    5 nind = nin                                                        
		      go to 9005                                                        
		   10 noutd = nout                                                      
		 9005 return                                                            
		      end                                                               	                                                            
end module