module cgr

	contains                                      
		subroutine zxcgr  (funct,m,n,acc,maxfn,dfpred,x,g,f,w,ier,iounit)          
		!                                  specifications for arguments         
		!   purpose             - a conjugate gradient algorithm for finding
		!                           the minimum of a function of n variables
		!
		!   usage               - call zxcgr (funct,n,acc,maxfn,dfpred,x,g,f,w,
		!                           ier)
		!
		!   arguments    funct  - a user supplied subroutine which calculates
		!                           the objective function and its gradient
		!                           for given parameter values
		!                           x(1),x(2),...,x(n).
		!                           the calling sequence has the following form
		!                           call funct (n,x,f,g)
		!                           where x and g are vectors of length n.
		!                           the scalar f is for the objective function.
		!                           g(1), g(2), ..., g(n) are for the components
		!                           of the gradient of f.
		!                           funct must appear in an external statement
		!                           in the calling program. funct must not
		!                           alter the values of x(i),i=1,...,n or n.
		!                n      - the number of parameters of the objective
		!                           function. (input) (i.e.,the length of x)
		!                m      - the number of exponentials in funct
		!                           note: changed for readability
		!                acc    - convergence criterion. (input)
		!                           the calculation ends when the sum of squares
		!                           of the components of g is less than acc.
		!                maxfn  - maximum number of function evaluations (i.e.,
		!                           calls to subroutine funct) allowed. (input)
		!                           if maxfn is set to zero, then there is
		!                           no restriction on the number of function
		!                           evaluations.
		!                dfpred - a rough estimate of the expected reduction
		!                           in f, which is used to determine the size
		!                           of the initial change to x. (input)
		!                           note that dfpred is the expected reduction
		!                           itself, and does not depend on any ratios.
		!                           a bad value of dfpred causes an error
		!                           message, with ier=129, and a return on the
		!                           first iteration. (see the description of
		!                           ier below)
		!                x      - vector of length n containing parameter
		!                           values.
		!                         on input, x must contain the initial
		!                           parameter estimates.
		!                         on output, x contains the final parameter
		!                           estimates as determined by zxcgr.
		!                g      - a vector of length n containing the
		!                           components of the gradient of f at the
		!                           final parameter estimates. (output)
		!                f      - a scalar containing the value of the function
		!                           at the final parameter estimates. (output)
		!                w      - work vector of length 6*n.
		!                ier    - error parameter. (output)
		!                         ier = 0 implies that convergence was
		!                           achieved and no errors occurred.
		!                         terminal error
		!                           ier = 129 implies that the line search of
		!                             an integration was abandoned. this
		!                             error may be caused by an error in the
		!                             gradient.
		!                           ier = 130 implies that the calculation
		!                             cannot continue because the search
		!                             direction is uphill.
		!                           ier = 131 implies that the iteration was
		!                             terminated because maxfn was exceeded.
		!                           ier = 132 implies that the calculation
		!                             was terminated because two consecutive
		!                             iterations failed to reduce f.
		!
		!   precision/hardware  - single and double/h32
		!                       - single/h36,h48,h60
		!
		!   reqd. imsl routines - uertst,ugetio
		!
		!   notation            - information on special notation and
		!                           conventions is available in the manual
		!                           introduction or through imsl routine uhelp
		!
		!   remarks  1.  the routine includes no thorough checks on the part
		!                of the user program that calculates the derivatives
		!                of the objective function. therefore, because
		!                derivative calculation is a frequent source of
		!                error, the user should verify independently the
		!                correctness of the derivatives that are given to
		!                the routine.
		!            2.  because of the close relation between the conjugate
		!                gradient method and the method of steepest descents,
		!                it is very helpful to choose the scale of the
		!                variables in a way that balances the magnitudes of
		!                the components of a typical derivate vector. it
		!                can be particularly inefficient if a few components
		!                of the gradient are much larger than the rest.
		!            3.  if the value of the parameter acc in the argument
		!                list of the routine is set to zero, then the
		!                subroutine will continue its calculation until it
		!                stops reducing the objective function. in this case
		!                the usual behaviour is that changes in the
		!                objective function become dominated by computer
		!                rounding errors before precision is lost in the
		!                gradient vector. therefore, because the point of
		!                view has been taken that the user requires the
		!                least possible value of the function, a value of
		!                the objective function that is small due to
		!                computer rounding errors can prevent further
		!                progress. hence the precision in the final values
		!                of the variables may be only about half the
		!                number of significant digits in the computer
		!                arithmetic, but the least value of f is usually
		!                found to quite high accuracy.
		!                                                                            
		!      implicit double precision (a-h,o-z)
		      implicit none
		      
		      external funct
		      integer            m,n,maxfn,ier                                    
		      real*8             acc,dfpred,x(n),g(n),f,w(1)                    
		!                                  specifications for local variables   
		      integer            maxlin,mxfcon,i,iginit,igopt,iretry,irsdg,  &   
		     &                   irsdx,iterc,iterfm,iterrs,ixopt,ncalls,nfbeg, & 
		     &                   nfopt,iounit                                        
		      real*8             beta,ddspln,dfpr,fch,finit,fmin,gamden,gama,   &
		     &                   ginit,gmin,gnew,gspln,gsqrd,sbound,step,stepch,&
		     &                   stmin,sum,work                                 
		      data               maxlin/5/,mxfcon/2/                            
		!                                  first executable statement           
		      ier = 0                                                           
		      irsdx = n                                                         
		      irsdg = irsdx+n                                                   
		      iginit = irsdg+n                                                  
		      ixopt = iginit+n                                                  
		      igopt = ixopt+n                                                   
		      iterc = 0                                                         
		      ncalls = 0                                                        
		      iterfm = iterc                                                    
		    5 ncalls = ncalls+1                                                 
		!      print *,'cgr ',x(1:n)
		      call funct (n,m,x,f,g)                                              
		      if (ncalls.ge.2) go to 20                                         
		   10 do   i=1,n                                                       
		        w(i) = -g(i)
		      enddo                                                      
		      iterrs = 0                                                        
		      if (iterc.gt.0) go to 80                                          
		   20 gnew = 0.0                                                        
		      sum = 0.0                                                         
		      do   i=1,n                                                       
		         gnew = gnew+w(i)*g(i)                                          
		        sum = sum+g(i)**2      
		      enddo                                           
		      if (ncalls.eq.1) go to 35                                         
		      fch = f-fmin                                                      
		      if (fch) 35,30,50                                                 
		   30 if (gnew/gmin.lt.-1.0) go to 45                                   
		   35 fmin = f                                                          
		      gsqrd = sum                                                       
		      nfopt = ncalls                                                    
		      do  i=1,n                                                       
		        w(ixopt+i) = x(i)                                              
		        w(igopt+i) = g(i)                                                 
		      enddo                                           
		   45 if (sum.le.acc) go to 9005                                        
		   50 if (ncalls.ne.maxfn) go to 55                                     
		      ier = 131                                                         
		      go to 9000                                                        
		   55 if (ncalls.gt.1) go to 100                                        
		      dfpr = dfpred                                                     
		      stmin = dfpred/gsqrd                                              
		   80 iterc = iterc+1                                                   
		      finit = f                                                         
		      ginit = 0.0                                                       
		      do  i=1,n                                                       
		        w(iginit+i) = g(i)                                             
		        ginit = ginit+w(i)*g(i)                                           
		      enddo                                           
		      if (ginit.ge.0.0) go to 165                                       
		      gmin = ginit                                                      
		      sbound = -1.0                                                     
		      nfbeg = ncalls                                                    
		      iretry = -1                                                       
		      stepch = dmin1(stmin,abs(dfpr/ginit))                             
		      stmin = 0.0                                                       
		   90 step = stmin+stepch                                               
		      work = 0.0                                                        
		      do  i=1,n                                                       
		        x(i) = w(ixopt+i)+stepch*w(i)                                  
		        work = dmax1(work,abs(x(i)-w(ixopt+i)))                           
		      enddo                                           
		      if (work.gt.0.0) go to 5                                          
		      if (ncalls.gt.nfbeg+1) go to 115                                  
		      if (abs(gmin/ginit)-0.2) 170,170,115                              
		  100 work = (fch+fch)/stepch-gnew-gmin                                 
		      ddspln = (gnew-gmin)/stepch                                       
		      if (ncalls.gt.nfopt) sbound = step                                
		      if (ncalls.gt.nfopt) go to 105                                    
		      if (gmin*gnew.le.0.0) sbound = stmin                              
		      stmin = step                                                      
		      gmin = gnew                                                       
		      stepch = -stepch                                                  
		  105 if (fch.ne.0.0) ddspln = ddspln+(work+work)/stepch                
		      if (gmin.eq.0.0) go to 170                                        
		      if (ncalls.le.nfbeg+1) go to 120                                  
		      if (abs(gmin/ginit).le.0.2) go to 170                             
		  110 if (ncalls.lt.nfopt+maxlin) go to 120                             
		  115 ier = 129                                                         
		      go to 170                                                         
		  120 stepch = 0.5*(sbound-stmin)                                       
		      if (sbound.lt.-0.5) stepch = 9.0*stmin                            
		      gspln = gmin+stepch*ddspln                                        
		      if (gmin*gspln.lt.0.0) stepch = stepch*gmin/(gmin-gspln)          
		      go to 90                                                          
		  125 sum = 0.0                                                         
		      do  i=1,n                                                      
		        sum = sum+g(i)*w(iginit+i)  
		      enddo                                      
		      beta = (gsqrd-sum)/(gmin-ginit)                                   
		      if (abs(beta*gmin).le.0.2*gsqrd) go to 135                        
		      iretry = iretry+1                                                 
		      if (iretry.le.0) go to 110                                        
		  135 if (f.lt.finit) iterfm = iterc                                    
		      if (iterc.lt.iterfm+mxfcon) go to 140                             
		      ier = 132                                                         
		      go to 9000                                                        
		  140 dfpr = stmin*ginit                                                
		      if (iretry.gt.0) go to 10                                         
		      if (iterrs.eq.0) go to 155                                        
		      if (iterc-iterrs.ge.n) go to 155                                  
		      if (abs(sum).ge.0.2*gsqrd) go to 155                              
		      gama = 0.0                                                        
		      sum = 0.0                                                         
		      do  i=1,n                                                      
		        gama = gama+g(i)*w(irsdg+i)                                    
		        sum = sum+g(i)*w(irsdx+i)
		      enddo                                        
		      gama = gama/gamden                                                
		      if (abs(beta*gmin+gama*sum).ge.0.2*gsqrd) go to 155               
		      do  i=1,n                                                      
		        w(i) = -g(i)+beta*w(i)+gama*w(irsdx+i)  
		      enddo                          
		      go to 80                                                          
		  155 gamden = gmin-ginit                                               
		      do  i=1,n                                                      
		        w(irsdx+i) = w(i)                                              
		        w(irsdg+i) = g(i)-w(iginit+i)                                  
		        w(i) = -g(i)+beta*w(i)
		      enddo                                            
		      iterrs = iterc                                                    
		      go to 80                                                          
		  165 ier = 130                                                         
		  170 if (ncalls.eq.nfopt) go to 180                                    
		      f = fmin                                                          
		      do i=1,n                                                      
		        x(i) = w(ixopt+i)                                              
		        g(i) = w(igopt+i)
		      enddo                                            
		  180 if (ier.eq.0) go to 125                                           
		 9000 continue                                                          
		
		!      call funct (n,x,f,g)                                              
		!      sum = 0.0                                                         
		!      do   i=1,n                                                       
		!        sum = sum+g(i)**2      
		!      enddo                                           
		!      if(ier.eq.129)print *,ier,acc,sum,x,f,g
		
		      call uertst (ier,'zxcgr ',iounit)
		 9005 return                                                            
		      end                                                               
		! deck uertst
		!   imsl routine name   - uertst                                        
		      subroutine uertst (ier,name,iounit)                                      
		      implicit double precision (a-h,o-z)
		      integer            ier                                            
		      character (len=6)  ::name,namset,nameq                                 
		      integer            ieq,ieqdf,iounit,level,levold                   
		      data               namset/'uerset'/                
		      data               nameq /'      '/                                   
		      data               level/4/,ieqdf/0/,ieq/1h=/                     
		!      call ugetio(1,nin,iounit)
		
		!      iounit=91  ! to avoid too much outpu 
		
		      if (ier.gt.999) go to 25                                          
		      if (ier.lt.-32) go to 55                                          
		      if (ier.le.128) go to 5                                           
		      if (level.lt.1) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,35) ier,nameq,ieq,name            
		      if (ieqdf.eq.0) write(iounit,35) ier,name                       
		      go to 30                                                          
		    5 if (ier.le.64) go to 10                                           
		      if (level.lt.2) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,40) ier,nameq,ieq,name             
		      if (ieqdf.eq.0) write(iounit,40) ier,name                      
		      go to 30                                                          
		   10 if (ier.le.32) go to 15                                           
		      if (level.lt.3) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,45) ier,nameq,ieq,name            
		      if (ieqdf.eq.0) write(iounit,45) ier,name                      
		      go to 30                                                          
		   15 continue                                                          
		      if (name.ne.namset) go to 25                           
		      levold = level                                                    
		      level = ier                                                       
		      ier = levold                                                      
		      if (level.lt.0) level = 4                                         
		      if (level.gt.4) level = 4                                         
		      go to 30                                                          
		   25 continue                                                          
		      if (level.lt.4) go to 30                                          
		      if (ieqdf.eq.1) write(iounit,50) ier,nameq,ieq,name            
		      if (ieqdf.eq.0) write(iounit,50) ier,name                       
		   30 ieqdf = 0                                                         
		      return                                                            
		   35 format(19h *** terminal error,10x,7h(ier = ,i3,   &                
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   40 format(27h *** warning with fix error,2x,7h(ier = ,i3,  &          
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   45 format(18h *** warning error,11x,7h(ier = ,i3,       &             
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   50 format(20h *** undefined error,9x,7h(ier = ,i5,     &              
		     &       20h) from imsl routine ,a6,a1,a6)                        
		   55 ieqdf = 1                                                         
		      nameq= name
		   65 return                                                            
		      end                                                               
		! deck ugetio
		!   imsl routine name   - ugetio                                        
		      subroutine ugetio(iopt,nin,nout)                                  
		      implicit double precision (a-h,o-z)
		      integer            iopt,nin,nout                                  
		      integer            nind,noutd                                     
		      data               nind/5/,noutd/6/                               
		      if (iopt.eq.3) go to 10                                           
		      if (iopt.eq.2) go to 5                                            
		      if (iopt.ne.1) go to 9005                                         
		      nin = nind                                                        
		      nout = noutd                                                      
		      go to 9005                                                        
		    5 nind = nin                                                        
		      go to 9005                                                        
		   10 noutd = nout                                                      
		 9005 return                                                            
		      end                                                               	                                                            
end module

module fit_functions
	use cgr
	implicit none
	save
	real(8),allocatable,dimension(:)::loc_ev,loc_sigma
	real(8),allocatable,dimension(:,:)::loc_invcorr
	integer::tmax,tlower,tupper,iounit,ntsymm,nexp

	contains
		! subroutine func_exp(n,p,f,g) is called by zxcgr
		! subroutine fit_exp_n(ev,invcorr,tlowin,tmaxin,p,nx,io) does the fit
		! function chi2_f_exp(ev,invcov,tlowin,tmaxin,p,nexp) gets chi2
		! calls zxcgr with func_xxx
		
		subroutine fit_exp_n(ev,invcorr,tlowin,tmaxin,p,nx,sigma)
		
				! fit to one exponential a exp(-mass t) 
				! in the range t=1..tmaxin
				implicit none
		
				integer,intent(in)::tlowin,tmaxin,nx
				real(8),intent(in),dimension(tmaxin)::ev,sigma
				real(8),intent(in),dimension(tmaxin,tmaxin)::invcorr
				real(8),dimension(2*nx),intent(inout)::p
				real(8),dimension(:),allocatable::g,work,psave
				real(8)::chi2,dfpred,acc,chi2old
				integer::iter,ierr,npar,ti
		
				nexp=nx
				npar=2*nexp
				tmax=tmaxin
				tlower=tlowin
				allocate(loc_ev(tmaxin),loc_sigma(tmaxin),loc_invcorr(tmaxin,tmaxin),g(npar),work(12*npar),psave(npar))
				loc_ev=ev
				loc_invcorr=invcorr
				loc_sigma=sigma
		
				! sensible p or just zero?
				if(p(2).eq.0)then
				  p(1)=0.4d0
				  p(2)=0.5d0
				  if(nx.eq.2) then
				    p(3)=0.4d0
				    p(4)=0.8d0
				  end if
				end if
				psave=p
		
				call func_exp(npar,nexp,psave,chi2old,g)
		
				! estimate start value from lowest t points
				ti=(1+tmaxin)/2
				select case(nexp)
				case (1)
				  p(2)=-log(ev(tmaxin-1)/ev(1+1))/(tmaxin-1-2)
				  p(1)=ev(ti)*exp(p(2)*ti)
				case (2)
				  p(2)=-log(ev(tmaxin-1)/ev(1+1))/(tmaxin-1-2)
				  p(1)=ev(ti)*exp(p(2)*ti)
				  p(4)=p(2)+0.4d0
				  p(3)=p(1)+0.1d0
				case default
				  write(*,*) 'This case :',nexp,' is not implemented!'
				  stop
				end select
				call func_exp(npar,nexp,p,chi2,g)

				if(chi2.gt.chi2old) p=psave
		
				dfpred=0.001d0   !may adjust to size of largest sigma?
				iter=2000
				acc=1.d-8
		
				call zxcgr(func_exp,nexp,npar,acc,iter,dfpred,p,g,chi2,work,ierr,1333)          
				if(ierr.ne.0)then
		  		  write(132,'(a,e15.7,4i5)')'zxcgr did not converge ',chi2,ierr,1,tmaxin
				else 
				  write(132,'(a,e15.7)')    'zxcgr did converge to  ',chi2
				end if
		
				deallocate(loc_ev,loc_invcorr,loc_sigma,g,work)
						
		end subroutine fit_exp_n
		
		subroutine func_exp(n,nexp,pin,f,g)
				! p(i) for n=4 are in log convention "external"

				implicit none
				
				integer,intent(in)::n,nexp
				real(8),intent(in),dimension(n)::pin
				real(8),intent(out),dimension(n)::g
				real(8),intent(out)::f
				real(8),dimension(4)::p
				integer::t,ts,ndf
				
				if(pin(1).gt.1.d20)then
				write(132,*)'SR func_exp emergecy: input parameter',p
				end if
				f=0

				select case(nexp)
				
				case (1)
				p(1)=pin(1)
				p(2)=pin(2)
				do t=1,tmax
				do ts=1,tmax
				  f=f + (p(1)*exp(-p(2)*t)-loc_ev(t))*&
				  &     (p(1)*exp(-p(2)*ts)-loc_ev(ts))*&
				  &     loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
				end do
				end do
				ndf=tmax-2
				case (2)
				p=pin

				do t=1,tmax
				do ts=1,tmax
				  f=f + (p(1)*exp(-p(2)*t) +p(3)*exp(-p(4)*t)-loc_ev(t))*&
				  &     (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
				  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
				end do
				end do
				ndf=tmax-4
				case default
				  write(132,*)'SR func_exp: case not implemented!'
				  stop
				end select
				
				f=f/ndf
				if(f.gt.1.d15)then
				  write(132,'(a)')'SR func_exp emergency: result reset because too large:',f
				  f=1.d15+20.d0*dlog(f)
				end if
				
				g=0
				
				select case (nexp)
				
				case (1)
				  do t=1,tmax
				    do ts=1,tmax
				      g(1)=g(1) + (  exp(-p(2)*t)*(p(1)*exp(-p(2)*ts)-loc_ev(ts))&
				      &                  +(p(1)*exp(-p(2)*t) -loc_ev(t))*exp(-p(2)*ts)  )*&
				      &                   loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
				
				      g(2)=g(2) + ( -t*p(1)*exp(-p(2)*t)*(p(1)*exp(-p(2)*ts)-loc_ev(ts))&
					  &                  -(p(1)*exp(-p(2)*t) -loc_ev(t))*ts*p(1)*exp(-p(2)*ts)  )*&
					  &                   loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
				    end do
				  end do
				  ndf=tmax-2

				case (2)
				  do t=1,tmax
				    do ts=1,tmax
					  g(1)=g(1)+& 
					  &         exp(-p(2)*t)*&
					  &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
					  &        +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
					  &         exp(-p(2)*ts)*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
					  
					  g(3)=g(3)+& 
					  &          exp(-p(4)*t)*&
					  &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
					  &        +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
					  &         exp(-p(4)*ts)*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
				
					  g(2)=g(2)+&
					  &         (p(1)*exp(-p(2)*t)*(-t))*&
					  &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
					  &         +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
					  &         (p(1)*exp(-p(2)*ts)*(-ts))*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
				
					  g(4)=g(4)+&
					  &         (p(3)*exp(-p(4)*t)*(-t))*&
					  &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-loc_ev(ts))*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))&
					  &         +(p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-loc_ev(t))*&
					  &         (p(3)*exp(-p(4)*ts)*(-ts))*&
					  &         loc_invcorr(t,ts)/(loc_sigma(t)*loc_sigma(ts))
					end do
				  end do
				  ndf=tmax-4
				end select
				
				g=g/ndf		
		end subroutine func_exp
		
		function chi2_f_exp(ev,invcov,tlowin,tmaxin,p,nexp,sigma)
				
				! Here p(i) are defined like in the funtion
				! p(1)+exp(-p(2)*t) + p(3)* exp(-p(4)*t)
				
				implicit none
				
				integer,intent(in)::tlowin,tmaxin,nexp
				real(8),intent(in),dimension(2*nexp)::p
				real(8),intent(in),dimension(tmaxin)::ev,sigma
				real(8),intent(in),dimension(tmaxin,tmaxin)::invcov
				real(8) ::chi2_f_exp
				integer::t,ts,ndf
				real(8)::sum
				
				sum=0
				select case(nexp)
				
				case (1)
				
					do t=1,tmaxin
						do ts=1,tmaxin
						  sum=sum + (p(1)*exp(-p(2)*t)-ev(t))*&
						  &         (p(1)*exp(-p(2)*ts)-ev(ts))*&
						  &         invcov(t,ts)/(sigma(t)*sigma(ts))
						end do
					end do
					ndf=tmaxin-2
				case (2)
					do t=1,tmaxin
						do ts=1,tmaxin
						  sum=sum + (p(1)*exp(-p(2)*t)+p(3)*exp(-p(4)*t)-ev(t))*&
						  &         (p(1)*exp(-p(2)*ts)+p(3)*exp(-p(4)*ts)-ev(ts))*&
						  &         invcov(t,ts)/(sigma(t)*sigma(ts))
						end do
					end do
					ndf=tmaxin-4
				
				end select
				
				chi2_f_exp=sum/ndf
		end function chi2_f_exp
end module fit_functions

module inversion

	contains

	!=======================================================================
      SUBROUTINE INVERT (A,N,D,IFAIL,L,M,EPS)                           
	!=======================================================================
      IMPLICIT NONE
      integer, parameter :: double=selected_real_kind(p=14,r=30)
      integer, parameter :: single=selected_real_kind(6)
      real (8), dimension(n,n) ::A
      integer, dimension(n)  ::L,M 
      real (8)     :: d,eps,pivot,biga,hold
      integer                :: n,ifail,k,i,j
	!     On exit:
	!              ifail=0  ok
	!              ifail=1 error
	!=======================================================================
      D=1.0d0 
      PIVOT=0.0d0 
      ifail = 0   

      DO K=1,N   

        L(K)=K
        M(K)=K
        BIGA=A(K,K)
        DO I=K,N
          DO J=K,N
            IF(ABS (BIGA).lt.ABS (A(I,J))) then
              BIGA = A(I,J)
              L(K)=I
              M(K)=J
            endif
          enddo
        enddo
        IF(ABS (BIGA).gt.ABS(PIVOT)) PIVOT=BIGA
        IF (ABS (BIGA/PIVOT).lt.EPS) then
          ifail = 1
          RETURN
        endif
        J=L(K)
        IF(L(K).gt.K)then
          DO I=1,N
            HOLD = -A(K,I)
            A(K,I)=A(J,I)
            A(J,I)=HOLD
          enddo
        endif
        I=M(K)
        IF(M(K).gt.K)then
          DO J=1,N
            HOLD=-A(J,K)
            A(J,K) = A(J,I)
            A(J,I) = HOLD
          enddo
        endif
 
        DO I=1,N
          IF(I.ne.K) A(I,K)=A(I,K)/(-A(K,K))
        enddo
 
        DO I=1,N
          DO J=1,N
            IF((I.ne.K) .and.(J.ne.K)) A(I,J) = A(I,K)*A(K,J)+A(I,J)
          enddo
        enddo
 
        DO  J=1,N
          IF(J.ne.K) A(K,J) = A(K,J)/A(K,K)
        enddo
 
        D=D*A(K,K)
        A(K,K) = 1.0d0/A(K,K)
 
      enddo

      K = N
  100 K = K-1
      IF(K.le.0) goto 150
      I=L(K)
 
      IF(I.gt.K) then
        DO J=1,N
          HOLD = A(J,K)
          A(J,K) = -A(J,I)
          A(J,I) = HOLD
        enddo
      endif
 
      J = M(K)

      IF(J.gt.K) then
        DO  I=1,N
          HOLD = A(K,I)
          A(K,I) = -A(J,I)
          A(J,I) = HOLD
        enddo
      endif
 
      GOTO 100

  150 RETURN

      END SUBROUTINE INVERT
end module inversion

module fitting

	implicit none
	contains

	!########################################!
	subroutine do_exp_fit(result_file,eigval,eigval_avg,eigval_jack,nkonf,nt,tzero,t1,t2,svdcut,pstart)
		!eigval and eigval avg are used calculate the correlation matrix, eigval_jack is used to fit
		use fit_functions
		use inversion
		use cgr
		implicit none
	
		integer,intent(in)::nt,nkonf,tzero
		integer,intent(in)::t1,t2
		real(8),dimension(nt,nkonf),intent(in)::eigval
		real(8),dimension(nt),intent(in)::eigval_avg
		real(8),dimension(nt),intent(in)::eigval_jack
		character(len=*), intent(in) :: result_file
		real(8),dimension(:,:),allocatable::eigval_correl
		real(8),dimension(:,:),allocatable:: correl,covar_save,ic
		real(8),dimension(:,:),allocatable:: correla
		real(8),dimension(:),allocatable:: ev,eval,eveps,evx,sigma
		real(8),dimension(4),intent(in)::pstart
		real(8),dimension(4)::p,peps,pa,px,psave
		real(8),intent(in)::svdcut
		real(8)::correldet,chi2,chi2a,corrfac
		integer::ifail,i,j,n,t,t0,tlow,thigh,tdel,nop,igraph,ngraph
		integer,dimension(:),allocatable::lvec,mvec
		character(len=256)::fn
		real(8),dimension(:),allocatable :: work
		real(8),dimension(1) :: testwork
		integer::info,lwork,ne,ndf
		real(8),parameter::epssvd=1.d0
		logical::svd,corrflag
		character(len=1)::token
		integer :: efit_fileunit
	
		allocate(eval(nt),eveps(nt),lvec(nt),mvec(nt),eigval_correl(nt,nt))
		efit_fileunit=28
	    open(unit=efit_fileunit,file=trim(result_file),status="unknown",form="formatted")

		!initialize
		p=pstart
		psave=p

		eval(:)=eigval_avg(:)
		
		call mk_eigval_correl(eigval,eval,eigval_correl,nt,nkonf)

		t0=tzero
	
		do t=1,nt
	  		eveps(t)=sqrt(eigval_correl(t,t))
		end do

		tlow=t1
		thigh=t2

  		corrflag=.true.
  		tdel=thigh+1-tlow

  		if(tdel.lt.3)then
    		print *,'Combination ',tlow,thigh,' skipped: to few points'
    		goto 1
  		end if
	
		allocate(correl(tdel,tdel),covar_save(tdel,tdel),ic(tdel,tdel),ev(tdel),sigma(tdel))
		
		sigma(1:tdel)=eveps(tlow:tlow-1+tdel)
		!use normalized correlation matrix, correct chi2 later
		do i=1,tdel
			do j=1,tdel
  				correl(i,j)=eigval_correl(tlow-1+i,tlow-1+j)/(sigma(i)*sigma(j))
			end do!j
		end do!i
	
		covar_save=correl
	
		do i=1,tdel
  			ev(i)=eigval_jack(tlow-1+i)
		end do!i
	
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		!analyze cov matrix for stability and positivity
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
		allocate(correla(tdel,tdel),evx(tdel))
		correla=correl
		lwork=2*nt-1
		!real symm matrix: better use DSYEV
		!call ZHEEV( 'V','U',tdel, correla, tdel, evx, testwork,-1,rwork,info)
		call DSYEV( 'V','U',tdel, correla, tdel, evx, testwork,-1,info)
		if(info.eq.0)then
 			lwork=testwork(1)+1
		else
 			write(132,*)'do_exp_fit: DSYEV returned with info =',info
		end if

		allocate(work(lwork))
		correla=correl
		!call ZHEEV( 'V','U',tdel, correla, tdel, evx, work,lwork,rwork,info)
		call DSYEV( 'V','U',tdel, correla, tdel, evx, work,lwork,info)
		!deallocate(work,rwork)
		deallocate(work)
		
		if(info.ne.0)then
	  		write(132,*)'ERROR in do_exp_fit for correl diagonalization with info =', info
	  		stop
		end if
	
		! the svd criterion should be better : exp decay possible and ok
		! if too fast take mean of lowest eigenvalues
	
		svd=.false.
		!svdslope=log(maxval(abs(evx))/minval(abs(evx)))/(tdel-1)
	
		if((tdel.gt.8).and.(any(evx.le.0).or.(minval(abs(evx)).lt.svdcut)))then		
			write(132,*)'do_exp_fit: covariance matrix badly tuned for tlow, thigh=',tlow,thigh
			write(132,'(a,/,5(8e12.3))')' lambda(correl) =',evx(1:tdel)
			write(132,*)'svd approximation, eigenvalues retuned'
			svd=.true.
		end if
	
		if(svd)then
	
			covar_save=0
			ndf=tdel     !counts the addition d.f. removed
			do n=1,tdel
				! svd:
				! add the projector to inverse
	  			if(evx(n).gt.svdcut)then
	    			ndf=ndf-1
	    			do i=1,tdel
	    				do j=1,tdel
	     					covar_save(i,j)=covar_save(i,j)+correla(i,n)*correla(j,n)/evx(n)
	    				end do
	    			end do
	  			end if
			end do
	
		else
	
			ndf=0
			covar_save=correl
			call invert (covar_save,tdel,correldet,ifail,lvec,mvec,1.d-8)  
			if(ifail.ne.0) then
	  			write(132,*)'inversion failed for nop =',nop,' and det= ',correldet
	  			write(132,*)'used diagonal cov (uncorrelated fit) instead'
	  			corrflag=.false.
	  		goto 5
		end if
	5 continue
		end if
	
		deallocate(correla)
	
		!!!!!!!!!!!!!!!!!!!!!!
		! 2 exp fits
		!!!!!!!!!!!!!!!!!!!!!!
	
		if(tdel.lt.5)then
	  		write(132,*)'Combination ',tlow,thigh,' skipped for 2 exp fit'
  			goto 2
		end if
	
		ne=2
		!!!!!!!!!!!!!!!!!!!!!!
		! 2 exp fit uncorrelated
		!!!!!!!!!!!!!!!!!!!!!!
	
		ic(:,:)=0
		do t=1,tdel
  			ic(t,t)=1.d0/correl(t,t)
		end do

		p=psave
		call fit_exp_n(ev,ic,tlow,tdel,p,ne,sigma)
		if(p(4).lt.p(2))then
  			pa(1:2)=p(3:4)
  			p(3:4)=p(1:2)
  			p(1:2)=pa(1:2)
		end if
		chi2=chi2_f_exp(ev,ic,tlow,tdel,p,ne,sigma)
		px=pstart
		call fit_exp_n(ev,ic,tlow,tdel,px,ne,sigma)
		if(px(4).lt.px(2))then
  			pa(1:2)=px(3:4)
  			px(3:4)=px(1:2)
  			px(1:2)=pa(1:2)
		end if
		chi2a=chi2_f_exp(ev,ic,tlow,tdel,px,ne,sigma)
		if(chi2a.lt.chi2)then
			p=px
			chi2=chi2a
		end if
	
		psave=p
	
		token='.'

		if(chi2.gt.3.0)token='*'

		write(efit_fileunit,'(1x,a1,1x,a1,4i5,3x,e14.7,4(5x,e16.8,3x,e16.8))')token,'u',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
		p=psave
		
		if(corrflag)then
			
			!!!!!!!!!!!!!!!!!!!!!!
			! 2 exp fit correlated
			!!!!!!!!!!!!!!!!!!!!!!
			
			ic=covar_save
		
			p=psave
			call fit_exp_n(ev,ic,tlow,tdel,p,ne,sigma)
			if(p(4).lt.p(2))then
		  		pa(1:2)=p(3:4)
		  		p(3:4)=p(1:2)
		  		p(1:2)=pa(1:2)
			end if
			chi2=chi2_f_exp(ev,ic,tlow,tdel,p,ne,sigma)
			px=pstart
			call fit_exp_n(ev,ic,tlow,tdel,px,ne,sigma)
			if(px(4).lt.px(2))then
	 		    pa(1:2)=px(3:4)
	 		    px(3:4)=px(1:2)
				px(1:2)=pa(1:2)
			end if
			chi2a=chi2_f_exp(ev,ic,tlow,tdel,px,ne,sigma)
			if(chi2a.lt.chi2)then
				p=px
				chi2=chi2a
			end if
			psave=p
			corrfac=1.
			if(tdel-4-ndf.gt.1)corrfac=real(tdel-4)/real(tdel-4-ndf)
			chi2=chi2*corrfac
			
			token='.'
			if(chi2.gt.3.0)token='*'
			if(svd)token='S'
			write(efit_fileunit,'(1x,a1,1x,a1,4i5,3x,e14.7,4(5x,e16.8,3x,e16.8))')token,'c',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
			!write(*,'(1x,a1,1x,a1,4i5,3x,e14.7,4(5x,e16.8,3x,e16.8))')token,'c',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
			write(*,'(1x,a1,1x,a1,4i5,3x,f14.7,4(5x,f16.8,3x,f16.8))')token,'c',nop,ne,tlow-1,thigh-1,chi2,(p(i),peps(i),i=1,2*ne)
			p=psave
		end if
			
2 continue
	
		deallocate(correl,covar_save,ic,ev,evx,sigma)
	
1 continue
	
	
		deallocate(lvec,mvec,eval,eveps)
	end subroutine do_exp_fit
	
	
  subroutine mk_eigval_correl(eigval,eigval_avg,eigval_correl,nt,nkonf)
	! compute covariance matrix using the overal mean
	! This is Cov^(-1); cf. Michael, PRD49(1994)2616
	implicit none
	
	integer,intent(in)::nt,nkonf
	real(8),dimension(nt,nkonf),intent(in)::eigval
	real(8),dimension(nt),intent(in)::eigval_avg
	real(8),dimension(nt,nt),intent(out)::eigval_correl
	integer::i,t1,t2,k
	
	eigval_correl=0.0d0
	
	do k=1,nkonf
	  do t1=1,nt
		  do t2=1,nt
		    eigval_correl(t1,t2)=eigval_correl(t1,t2)+(eigval(t1,k)-eigval_avg(t1))*(eigval(t2,k)-eigval_avg(t2))
	 	  end do
	  end do
	end do

	eigval_correl=eigval_correl*dble(nkonf-1)/dble(nkonf)
	
	end subroutine mk_eigval_correl
end module fitting

module many_things
	implicit none
	contains

	 subroutine read_eigenvalues(file,nt,nkonf,eigval)
	  implicit none
	   integer, intent(in) :: nt,nkonf
	   character(len=*),intent(in) :: file
	   integer :: konf,t,time,config
	   real(8), dimension(Nt,Nkonf),intent(out) :: eigval
	   real(8) :: holder
		
	   open(unit=901,file=trim(adjustl(file)),status='unknown',form='formatted')

		 eigval=0.0d0

	   do konf=1,Nkonf
	    do t=1,Nt
	          read(901,*) time, holder, config
	          eigval(t,konf)=holder
	          !write(44,'(I4,E28.15E2,I8)') time, eigval(t,konf),config
	    end do
	   end do
	  close(901)
	  return
	 end subroutine read_eigenvalues

	 subroutine read_eigenvalue_cv(file,nt,eigval)
	  implicit none
	   integer, intent(in) :: nt
	   character(len=*),intent(in) :: file
	   integer :: t,time,config
	   real(8), dimension(Nt),intent(out) :: eigval
	   real(8) :: holder
		
	   open(unit=901,file=trim(adjustl(file)),status='unknown',form='formatted')

		 eigval=0.0d0

	    do t=1,Nt
	          read(901,*) time, holder
	          eigval(t)=holder
	          !write(*,*) holder
	    end do
	  close(901)
	  return
	 end subroutine read_eigenvalue_cv

	 !-----------------------------------------------------------!
	 !================jackknife statistics routine===============!
	 !-----------------------------------------------------------!
	 subroutine jack_statistics(x_J,k,x_avg)
	  integer, intent(in) :: k
	  real(8), dimension(k), intent(in) :: x_J
	  real(8), intent(out) :: x_avg
	  integer :: i
	
	  x_avg=0.0d0
	  do i=1,k
	    x_avg=x_avg + x_J(i)
	  end do
	  x_avg=x_avg/dble(k)
	
	  return
	 end subroutine jack_statistics
	
end module
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
program cbl_fitter
use fitting
use many_things

integer, parameter :: nt=32
integer, parameter :: nkonf=279
integer, parameter :: tzero=2
integer :: t1,t2
character(len=2) :: t1in, t2in
character(len=15) :: p1in,p2in,p3in,p4in
character(len=256) :: result_file
character(len=256) :: eigval_loc,eigval_loc_cv,eigval_loc_jack
real(8), dimension(nt,nkonf) :: eigval
real(8), dimension(nt) :: eigval_cv
real(8), dimension(nt) :: eigval_jack
real(8) :: svdcut
real(8), dimension(4) ::  pstart
integer :: t,konf,time,config

svdcut=0.00000001d0

pstart(1)=0.00509249
pstart(2)=1.74677
pstart(3)=0.0215
pstart(4)=9.403

call getarg(1,eigval_loc)
call getarg(2,eigval_loc_cv)
call getarg(3,eigval_loc_jack)
call getarg(4,t1in)
call getarg(5,t2in)
call getarg(6,p1in)
call getarg(7,p2in)
call getarg(8,p3in)
call getarg(9,p4in)
!write(*,*) eigval_loc
!write(*,*) eigval_loc_cv

read(t1in,'(I4)') t1
read(t2in,'(I4)') t2
read(p1in,*) pstart(1)
read(p2in,*) pstart(2)
read(p3in,*) pstart(3)
read(p4in,*) pstart(4)

result_file='result'

eigval_avg=0.0d0

call read_eigenvalues(eigval_loc,nt,nkonf,eigval)
call read_eigenvalue_cv(eigval_loc_cv,nt,eigval_cv)
call read_eigenvalue_cv(eigval_loc_jack,nt,eigval_jack)

call do_exp_fit(result_file,eigval,eigval_cv,eigval_jack,nkonf,nt,tzero,t1,t2,svdcut,pstart)

end program cbl_fitter

