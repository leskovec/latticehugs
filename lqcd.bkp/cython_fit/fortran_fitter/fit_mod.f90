
module fit_mod

    use fitting_1exp
    use fitting_2exp
    use fitting_const

    implicit none

    contains

    subroutine fit_eigenvalue(nt,nkonf,tmin,tmax,pinit,eigval,eigval_cv,eigval_jack,res1expU,res1expC,res2expU,res2expC,chi2_1e,chi2_2e)

        implicit none

        integer, intent(in) :: nt
        integer, intent(in) :: nkonf
        integer, intent(in) :: tmin
        integer, intent(in) :: tmax
        real(8), dimension(4), intent(in) ::  pinit
        real(8), dimension(nkonf,nt), intent(in) :: eigval
        real(8), dimension(nt),intent(in) :: eigval_cv
        real(8), dimension(nt),intent(in) :: eigval_jack
        real(8), dimension(2), intent(out) :: res1expC
        real(8), dimension(2), intent(out) :: res1expU
        real(8), dimension(4), intent(out) :: res2expC
        real(8), dimension(4), intent(out) :: res2expU
        real(8), intent(out) :: chi2_1e,chi2_2e
        integer, parameter :: tzero=0
        integer, dimension(2) :: t1,t2
        character(len=2) :: t1in, t2in
        character(len=15) :: p1in,p2in,p3in,p4in
        character(len=256) :: result_file
        character(len=256) :: eigval_loc,eigval_loc_cv,eigval_loc_jack
        real(8), dimension(4) ::  pstart
        real(8) :: svdcut
        integer :: t,konf,time,config
        
        svdcut=0.00000001d0
        
        result_file='result'
        
        t1(1)=tmin
        t2(1)=tmax
        t1(2)=t1(1)
        t2(2)=t2(1)
            
        call do_exp_fit_1exp(result_file,eigval,eigval_cv,eigval_jack,nkonf,nt,tzero,t1,t2,svdcut,pstart,res1expC,res1expU,chi2_1e)
        call do_exp_fit_2exp(result_file,eigval,eigval_cv,eigval_jack,nkonf,nt,tzero,t1,t2,svdcut,pstart,res2expC,res2expU,chi2_2e)

    end subroutine fit_eigenvalue


    subroutine fit_Zfactor(nt,nkonf,tmin,tmax,pinit,eigval,eigval_cv,eigval_jack,resconstU,resconstC,chi2)

        implicit none

        integer, intent(in) :: nt
        integer, intent(in) :: nkonf
        integer, intent(in) :: tmin
        integer, intent(in) :: tmax
        real(8), dimension(1), intent(in) ::  pinit
        real(8), dimension(nkonf,nt), intent(in) :: eigval
        real(8), dimension(nt),intent(in) :: eigval_cv
        real(8), dimension(nt),intent(in) :: eigval_jack
        real(8), dimension(2), intent(out) :: resconstC
        real(8), dimension(2), intent(out) :: resconstU
        real(8), intent(out) :: chi2
        integer, parameter :: tzero=0
        integer, dimension(2) :: t1,t2
        character(len=2) :: t1in, t2in
        character(len=15) :: p1in,p2in,p3in,p4in
        character(len=256) :: result_file
        character(len=256) :: eigval_loc,eigval_loc_cv,eigval_loc_jack
        real(8), dimension(4) ::  pstart
        real(8) :: svdcut
        integer :: t,konf,time,config
        
        svdcut=0.00000001d0
        
        result_file='result2'
        
        t1(1)=tmin
        t2(1)=tmax
        t1(2)=t1(1)
        t2(2)=t2(1)
        
        call do_exp_fit_const(result_file,eigval,eigval_cv,eigval_jack,nkonf,nt,tzero,t1,t2,svdcut,pstart,resconstC,resconstU,chi2)
        
    end subroutine fit_Zfactor

end module fit_mod

