module inversion

	contains

	!=======================================================================
      SUBROUTINE INVERT (A,N,D,IFAIL,L,M,EPS)                           
	!=======================================================================
      IMPLICIT NONE
      integer, parameter :: double=selected_real_kind(p=14,r=30)
      integer, parameter :: single=selected_real_kind(6)
      real (8), dimension(n,n) ::A
      integer, dimension(n)  ::L,M 
      real (8)     :: d,eps,pivot,biga,hold
      integer                :: n,ifail,k,i,j
	!     On exit:
	!              ifail=0  ok
	!              ifail=1 error
	!=======================================================================
      D=1.0d0 
      PIVOT=0.0d0 
      ifail = 0   

      DO K=1,N   

        L(K)=K
        M(K)=K
        BIGA=A(K,K)
        DO I=K,N
          DO J=K,N
            IF(ABS (BIGA).lt.ABS (A(I,J))) then
              BIGA = A(I,J)
              L(K)=I
              M(K)=J
            endif
          enddo
        enddo
        IF(ABS (BIGA).gt.ABS(PIVOT)) PIVOT=BIGA
        IF (ABS (BIGA/PIVOT).lt.EPS) then
          ifail = 1
          RETURN
        endif
        J=L(K)
        IF(L(K).gt.K)then
          DO I=1,N
            HOLD = -A(K,I)
            A(K,I)=A(J,I)
            A(J,I)=HOLD
          enddo
        endif
        I=M(K)
        IF(M(K).gt.K)then
          DO J=1,N
            HOLD=-A(J,K)
            A(J,K) = A(J,I)
            A(J,I) = HOLD
          enddo
        endif
 
        DO I=1,N
          IF(I.ne.K) A(I,K)=A(I,K)/(-A(K,K))
        enddo
 
        DO I=1,N
          DO J=1,N
            IF((I.ne.K) .and.(J.ne.K)) A(I,J) = A(I,K)*A(K,J)+A(I,J)
          enddo
        enddo
 
        DO  J=1,N
          IF(J.ne.K) A(K,J) = A(K,J)/A(K,K)
        enddo
 
        D=D*A(K,K)
        A(K,K) = 1.0d0/A(K,K)
 
      enddo

      K = N
  100 K = K-1
      IF(K.le.0) goto 150
      I=L(K)
 
      IF(I.gt.K) then
        DO J=1,N
          HOLD = A(J,K)
          A(J,K) = -A(J,I)
          A(J,I) = HOLD
        enddo
      endif
 
      J = M(K)

      IF(J.gt.K) then
        DO  I=1,N
          HOLD = A(K,I)
          A(K,I) = -A(J,I)
          A(J,I) = HOLD
        enddo
      endif
 
      GOTO 100

  150 RETURN

      END SUBROUTINE INVERT
end module inversion
