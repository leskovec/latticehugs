from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

#run with: LDSHARED="icc -shared" CC="icc -xhost" python setup.py build_ext --inplace
setup(
  name = 'c_fit',
  ext_modules=[
    Extension('c_fit', ['c_fit.pyx'],include_dirs=[numpy.get_include()])
    ],
  cmdclass = {'build_ext': build_ext}
)
