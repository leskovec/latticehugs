import numpy as np
from scipy import optimize
from scipy.special import legendre

cimport numpy as np
cimport cython

cpdef cf_pstar2(np.float64_t E_CMS, np.float64_t mp1, np.float64_t mp2):
    pst2=(E_CMS**4 + (mp1**2 - mp2**2)**2 - 2*(E_CMS**2)*(mp1**2 + mp2**2))/(4.*E_CMS**2)
    return pst2

######################################################################################################

cpdef mk_correl(np.int tdel, np.int Nkonf, np.ndarray[np.float64_t, ndim=2] eigval,  np.ndarray[np.float64_t, ndim=1] eigval_avg):
    cdef eval_correl = np.zeros((tdel,tdel),dtype=np.float64)
    cdef Py_ssize_t n,t1,t2
    for konf in range(Nkonf):
        for t1 in range(tdel):
            for t2 in range(tdel):
                eval_correl[t1,t2] += (eigval[konf,t1] - eigval_avg[t1])*(eigval[konf,t2] - eigval_avg[t2])
    return eval_correl

cpdef mk_correl_alt(np.int tdel, np.int Nkonf, np.ndarray[np.float64_t, ndim=2] eigval,  np.ndarray[np.float64_t, ndim=1] eigval_avg, np.ndarray[np.float64_t, ndim=1] eigval_sig):
    cdef eval_correl = np.zeros((tdel,tdel),dtype=np.float64)
    cdef Py_ssize_t n,t1,t2
    for konf in range(Nkonf):
        for t1 in range(tdel):
            for t2 in range(tdel):
                eval_correl[t1,t2] += (eigval[konf,t1] - eigval_avg[t1])*(eigval[konf,t2] - eigval_avg[t2])/(eigval_sig[t1]*eigval_sig[t2])
    return eval_correl,eigval_sig

cpdef chi2_exp(np.ndarray[np.float64_t, ndim=1] p, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] sigmay,f, np.int t0,np.ndarray[np.float64_t, ndim=2] inv_cov, np.int tdel):
    #y,sigmay,f,t0,inv_cov,tdel = args
    cdef np.float64_t c2_sum=0.0e0
    cdef Py_ssize_t t1,t2
    for t1 in range(tdel):
        for t2 in range(tdel):
            c2_sum += (f(t1,t0,p) - y[t1])*inv_cov[t1,t2]*(f(t2,t0,p) - y[t2])\
            /(sigmay[t1]*sigmay[t2])
    cdef np.float64_t ndf=float(tdel-len(p))
    return c2_sum/ndf

cpdef f_one_exp(np.int x, np.int t0, np.ndarray[np.float64_t, ndim=1] p):
    return p[0]*np.exp(-p[1]*(x-t0))

cpdef f_two_exp(np.int x, np.int t0, np.ndarray[np.float64_t, ndim=1] p):
    return (1.0-p[0])*np.exp(-p[1]*(x-t0)) + p[0]*np.exp(-p[2]*(x-t0))

cpdef f_two_exp2(np.int x, np.int t0, np.ndarray[np.float64_t, ndim=1] p):
    return p[0]*np.exp(-p[1]*(x-t0)) + p[2]*np.exp(-p[3]*(x-t0))

cpdef fit_eigenvalue(str fit_type, np.int t0, np.int Nkonf, np.int tmin, np.int tmax, np.ndarray[np.float64_t, ndim=1] P_init, np.ndarray[np.float64_t, ndim=2] eigval, np.ndarray[np.float64_t, ndim=1] C_eigval_cv, np.ndarray[np.float64_t, ndim=1] C_eigval_jack):
    cdef Py_ssize_t tdel = tmax-tmin+1
    
    eCor = mk_correl(tdel,Nkonf,eigval,C_eigval_cv)
    
    sigma=np.zeros(tdel,dtype=np.float64)
    cdef Py_ssize_t t
    for t in range(tdel):
        sigma[t] = np.sqrt(eCor[t,t])

    Cor=np.zeros((tdel,tdel),dtype=np.float64)
    cdef Py_ssize_t t1,t2
    for t1 in range(tdel):
        for t2 in range(tdel):
            Cor[t1,t2]=eCor[t1,t2]/(sigma[t1]*sigma[t2])

    #calculate pseudoinverse of Cor
    invCor=np.linalg.pinv(Cor)
        
    if fit_type == '1exp':           
        #minimize chi2
        args = C_eigval_jack, sigma, f_one_exp, t0, invCor, tdel
        res = optimize.minimize(chi2_exp, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif fit_type == '2exp':               
        #minimize chi2
        args = C_eigval_jack, sigma, f_two_exp, t0, invCor, tdel
        res = optimize.minimize(chi2_exp, P_init,args=args,method='L-BFGS-B',options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif fit_type == '2exp2':
        #minimize chi2
        args = C_eigval_jack, sigma, f_two_exp2, t0, invCor, tdel
        res = optimize.minimize(chi2_exp, P_init,args=args,method='L-BFGS-B',options={'ftol' : 1e-9, 'gtol' : 1e-8})
    if res.success==True:
        chi2=res.fun
        out=res
        succ=True
    else:
        chi2=0.0
        out=res
        succ=False
    return chi2,out,succ

#######################################################################################################

cpdef chi2_linear(np.ndarray[np.float64_t, ndim=1] p, np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] sigmay,f,np.ndarray[np.float64_t, ndim=2] inv_cov, np.int N):
    #y,sigmay,f,t0,inv_cov,tdel = args
    cdef np.float64_t c2_sum=0.0
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            c2_sum += (f(x[t1],p) - y[t1])*inv_cov[t1,t2]*(f(x[t2],p) - y[t2])/(sigmay[t1]*sigmay[t2])
    cdef np.float64_t ndf=float(N-len(p))
    return c2_sum/ndf

cpdef f_linear(np.float64_t x, np.ndarray[np.float64_t, ndim=1] p):
    return p[0]*x + p[1]

cpdef fit_linear(np.int Nkonf,np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] P_init, np.ndarray[np.float64_t, ndim=2] y, np.ndarray[np.float64_t, ndim=1] y_cv, np.ndarray[np.float64_t, ndim=1] y_jack):
    cdef Py_ssize_t N=y_cv.shape[0]

    eCor = mk_correl(N,Nkonf,y,y_cv)
    
    sigma=np.zeros(N,dtype=np.float64)
    cdef Py_ssize_t t
    for t in range(N):
        sigma[t] = np.sqrt(eCor[t,t])

    Cor=np.zeros((N,N),dtype=np.float64)
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            Cor[t1,t2]=eCor[t1,t2]/(sigma[t1]*sigma[t2])

    #calculate pseudoinverse of Cor
    invCor=np.linalg.pinv(Cor)
        
    #minimize chi2
    args = x, y_jack, sigma, f_linear, invCor, N
    res = optimize.minimize(chi2_linear, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    if res.success==True:
        chi2=res.fun
        out=res
        succ=True
    else:
        chi2=0.0
        out=res
        succ=False
    return chi2,out,succ

#######################################################################################################

cpdef chi2_constant(np.float64_t p, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] sigmay,f,np.ndarray[np.float64_t, ndim=2] inv_cov, np.int N):
    #y,sigmay,f,t0,inv_cov,tdel = args
    cdef np.float64_t c2_sum=0.0
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            c2_sum += (f(p) - y[t1])*inv_cov[t1,t2]*(f(p) - y[t2])/(sigmay[t1]*sigmay[t2])
    cdef np.float64_t ndf=float(N-1)
    return c2_sum/ndf

cpdef f_constant(np.float64_t p):
    return p

cpdef fit_constant(np.int Nkonf, np.float64_t P_init, np.ndarray[np.float64_t, ndim=2] y, np.ndarray[np.float64_t, ndim=1] y_cv, np.ndarray[np.float64_t, ndim=1] y_jack):
    cdef Py_ssize_t N=y_cv.shape[0]

    eCor = mk_correl(N,Nkonf,y,y_cv)
    
    sigma=np.zeros(N,dtype=np.float64)
    cdef Py_ssize_t t
    for t in range(N):
        sigma[t] = np.sqrt(eCor[t,t])

    Cor=np.zeros((N,N),dtype=np.float64)
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            Cor[t1,t2]=eCor[t1,t2]/(sigma[t1]*sigma[t2])

    #calculate pseudoinverse of Cor
    invCor=np.linalg.pinv(Cor)
        
    #minimize chi2
    args = y_jack, sigma, f_constant, invCor, N
    res = optimize.minimize(chi2_constant, P_init,args=args,method='Nelder-Mead')#, options={'ftol' : 1e-9, 'gtol' : 1e-8})
    if res.success==True:
        chi2=res.fun
        out=res
        succ=True
    else:
        chi2=0.0
        out=res
        succ=False
    return chi2,out,succ

############################################################################
cpdef l_atan(np.float64_t x):
    y=np.arctan(x)
    if y<0.:
        y+=np.pi
    return y*180./np.pi

cpdef l_acot(np.float64_t x):
    y=np.arctan(1.0/x)
    if y<0.:
        y+=np.pi
    return y*180./np.pi

#model one, no NR
cpdef chi2_BW_one(np.ndarray[np.float64_t, ndim=1] p, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] sigmay,f,np.ndarray[np.float64_t, ndim=2] inv_cov, np.int N):
    #y,sigmay,f,t0,inv_cov,tdel = args
    cdef np.float64_t c2_sum=0.0
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            c2_sum += (f(x[t1], m1, m2 ,p) - y[t1])*inv_cov[t1,t2]*(f(x[t2], m1, m2 ,p) - y[t2])/(sigmay[t1]*sigmay[t2])
    cdef np.float64_t ndf=float(N-len(p))
    return c2_sum/ndf

cpdef Gamma1(np.float64_t g, np.float64_t p2, np.float64_t ss):
    gam = ((g**2)/(6.*np.pi))*((np.sqrt(p2**3))/(ss**2))
    return gam


cpdef f_BW_one(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    pp=cf_pstar2(x, m1, m2)
    gam=Gamma1(p[1],pp,x)
    res=(x*gam)/(p[0]**2 - x**2)
    return l_atan(res)

#model two, no NR
cpdef chi2_BW_two(np.ndarray[np.float64_t, ndim=1] p, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] sigmay,f,np.ndarray[np.float64_t, ndim=2] inv_cov, np.int N):
    #y,sigmay,f,t0,inv_cov,tdel = args
    cdef np.float64_t c2_sum=0.0
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            c2_sum += (f(x[t1], m1, m2 ,p) - y[t1])*inv_cov[t1,t2]*(f(x[t2], m1, m2 ,p) - y[t2])/(sigmay[t1]*sigmay[t2])
    cdef np.float64_t ndf=float(N-len(p))
    return c2_sum/ndf

cpdef Gamma2(np.float64_t g, np.float64_t mR, np.float64_t rs2, np.float64_t p2, np.float64_t ss, np.float64_t m1,np.float64_t m2):
    ppR=cf_pstar2(mR,m1,m2)
    barfac = (1.0 + ppR*(rs2))/(1.0 + p2*(rs2))
    gam = ((g**2)/(6.*np.pi))*((np.sqrt(p2**3))/(ss**2))*barfac
    return gam

cpdef f_BW_two(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    pp=cf_pstar2(x, m1, m2)
    gam=Gamma2(p[1],p[0],p[2],pp,x,m1,m2)
    res=(x*gam)/(p[0]**2 - x**2)
    return l_atan(res)

#non-resonant phase shifts
#hard sphere p-wave
cpdef f_NR_hs(np.float64_t pst, np.float64_t A, np.float64_t rc):
    cdef np.float64_t prc = pst*rc
    nom = np.arctan(prc) - prc
    den = 1.0+prc*np.arctan(prc)
    return l_atan(A*nom/den)

#linear in s
cpdef f_NR_lins(np.float64_t x, np.float64_t a, np.float64_t b):
    cdef np.float64_t tt = a+b*x**2
    return tt

#first order ERE
cpdef f_NR_ERE1(np.float64_t ss, np.float64_t m1, np.float64_t m2, np.float64_t a, np.float64_t r1):
    cdef np.float64_t sthres=(m1+m2)**2
    cdef np.float64_t t1 = (-2.0)/(a*np.sqrt(ss**2 - sthres))
    cdef np.float64_t t2 = r1*np.sqrt(ss**2 - sthres)/(4.0)
    return l_acot(t1 + t2)

#sums of BW and NR phases
#BW one
#BW one and hs
cpdef f_BW_onehs(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    d_R = f_BW_one(x, m1, m2, p)
    pst=np.sqrt(cf_pstar2(x,m1,m2))
    d_NR = f_NR_hs(pst,p[2],p[3])
    return d_R+d_NR

#BW one and lins
cpdef f_BW_onelins(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    d_R = f_BW_one(x, m1, m2, p)
    d_NR = f_NR_lins(x,p[2],p[3])
    return d_R+d_NR

#BW one and ERE1
cpdef f_BW_oneERE1(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    d_R = f_BW_one(x, m1, m2, p)
    d_NR = f_NR_ERE1(x,m1,m2,p[2],p[3])
    return d_R+d_NR
#BW two
cpdef f_BW_twohs(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    d_R = f_BW_two(x, m1, m2, p)
    pst=np.sqrt(cf_pstar2(x,m1,m2))
    d_NR = f_NR_hs(pst,p[3],p[4])
    return d_R+d_NR

#BW two and lins
cpdef f_BW_twolins(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    d_R = f_BW_two(x, m1, m2, p)
    d_NR = f_NR_lins(x,p[3],p[4])
    return d_R+d_NR

#BW two and ERE1
cpdef f_BW_twoERE1(np.float64_t x, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] p):
    d_R = f_BW_two(x, m1, m2, p)
    d_NR = f_NR_ERE1(x,m1,m2,p[3],p[4])
    return d_R+d_NR

##############
cpdef chi2_BWNR(np.ndarray[np.float64_t, ndim=1] p, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] sigmay,f,np.ndarray[np.float64_t, ndim=2] inv_cov, np.int N):
    #y,sigmay,f,t0,inv_cov,tdel = args
    cdef np.float64_t c2_sum=0.0
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            c2_sum += (f(x[t1], m1, m2 ,p) - y[t1])*inv_cov[t1,t2]*(f(x[t2], m1, m2 ,p) - y[t2])/(sigmay[t1]*sigmay[t2])
    cdef np.float64_t ndf=float(N-len(p))
    return c2_sum/ndf

##############

cpdef fit_BW_delta(str ftype, np.int Nkonf, np.float64_t m1, np.float64_t m2, np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] P_init, np.ndarray[np.float64_t, ndim=2] y, np.ndarray[np.float64_t, ndim=1] y_cv, np.ndarray[np.float64_t, ndim=1] y_jack):
    cdef Py_ssize_t N=y_cv.shape[0]

    eCor = mk_correl(N,Nkonf,y,y_cv)
    
    sigma=np.zeros(N,dtype=np.float64)
    cdef Py_ssize_t t
    for t in range(N):
        sigma[t] = np.sqrt(eCor[t,t])

    Cor=np.zeros((N,N),dtype=np.float64)
    cdef Py_ssize_t t1,t2
    for t1 in range(N):
        for t2 in range(N):
            Cor[t1,t2]=eCor[t1,t2]/(sigma[t1]*sigma[t2])

    #calculate pseudoinverse of Cor
    invCor=np.linalg.pinv(Cor)
    
    if ftype=='BW_one':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_one, invCor, N
        res = optimize.minimize(chi2_BW_one, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})

    elif ftype=='BW_two':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_two, invCor, N
        res = optimize.minimize(chi2_BW_two, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif ftype=='BW_one+hs':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_onehs, invCor, N
        res = optimize.minimize(chi2_BWNR, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif ftype=='BW_one+lins':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_onelins, invCor, N
        res = optimize.minimize(chi2_BWNR, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif ftype=='BW_one+ERE1':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_oneERE1, invCor, N
        res = optimize.minimize(chi2_BWNR, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif ftype=='BW_two+hs':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_twohs, invCor, N
        res = optimize.minimize(chi2_BWNR, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif ftype=='BW_two+lins':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_twolins, invCor, N
        res = optimize.minimize(chi2_BWNR, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})
    elif ftype=='BW_two+ERE1':
        #minimize chi2
        args = m1, m2, x, y_jack, sigma, f_BW_twoERE1, invCor, N
        res = optimize.minimize(chi2_BWNR, P_init,args=args,method='L-BFGS-B', options={'ftol' : 1e-9, 'gtol' : 1e-8})        
    if res.success==True:
        chi2=res.fun
        out=res
        succ=True
    else:
        chi2=1j
        out=res
        succ=False
    return chi2,out,succ
