'''
This is a Cython implementation of the jackknife routine used in
the lqcdscattering python scripts
'''

# cython: wraparound = False
# cython: boundscheck = False

cimport cython
import numpy as np
cimport numpy as np

ctypedef fused scalar_t:
    np.complex128_t
    np.float64_t

ctypedef fused array_t:
    np.ndarray[np.complex128_t, ndim=1]
    np.ndarray[np.float64_t, ndim=1]

ctypedef fused array2_t:
    np.ndarray[np.complex128_t, ndim=2]
    np.ndarray[np.float64_t, ndim=2]

cpdef jack_average(array_t x):
  #calculates central value
  cdef Py_ssize_t n = x.shape[0]
  avg = np.sum(x)/n
  return avg

cpdef jack_average_Nt(array2_t x):
  #calculates central value
  cdef Py_ssize_t n = x.shape[0]
  cdef Py_ssize_t nt = x.shape[1]
  cdef Py_ssize_t t
  DataType = x.dtype
    
  if DataType=='complex128':
      avg = np.zeros(nt,dtype='complex128')
  elif DataType=='float64':
      avg = np.zeros(nt,dtype='float64')
  
  for t in range(nt):
    avg[t]=np.sum(x[:,t])/n
  return avg


cpdef jack_sigma(array_t x):
  #calculates the jackknife estimate of the variance
  cdef Py_ssize_t n = x.shape[0]
  x_avg = np.sum(x)/n
  if x.dtype=='complex128':
    sigma_x2_R = np.sum(np.power(np.add(x.real, - x_avg.real),2))
    sigma_R = np.sqrt(sigma_x2_R)*np.sqrt((n-1.)/n)
    sigma_x2_I = np.sum(np.power(np.add(x.imag, - x_avg.imag),2))
    sigma_I = np.sqrt(sigma_x2_I)*np.sqrt((n-1.)/n)
    sigma = sigma_R + sigma_I*1j
  elif x.dtype=='float64':
    sigma_x2 = np.sum(np.power(np.add(x, - x_avg),2))
    sigma = np.sqrt(sigma_x2)*np.sqrt((n-1.)/n)
  return x_avg,sigma

cpdef jack_sigma_Nt(array2_t x):
  #works on array(Nkonf,Nt)
  #calculates the jackknife estimate of the variance
  cdef Py_ssize_t n = x.shape[0]
  cdef Py_ssize_t nt = x.shape[1]
  cdef Py_ssize_t t

  DataType = x.dtype
    
  if DataType=='complex128':
      x_avg = np.zeros(nt,dtype='complex128')
      sigma_x2_R = np.zeros(nt,dtype='float64')
      sigma_R = np.zeros(nt,dtype='float64')
      sigma_x2_I = np.zeros(nt,dtype='float64')
      sigma_I = np.zeros(nt,dtype='float64')
      for t in range(nt):
          x_avg[t] = np.sum(x[:,t])/n
          sigma_x2_R[t] = np.sum(np.power(np.add(x[:,t].real, - x_avg[t].real),2))
          sigma_R[t] = np.sqrt(sigma_x2_R[t])*np.sqrt((n-1.)/n)
          sigma_x2_I[t] = np.sum(np.power(np.add(x[:,t].imag, - x_avg[t].imag),2))
          sigma_I[t] = np.sqrt(sigma_x2_I[t])*np.sqrt((n-1.)/n)
      sigma=sigma_R + sigma_I*1j
  elif DataType=='float64':
      x_avg = np.zeros(nt,dtype='float64')
      sigma_x2 = np.zeros(nt,dtype='float64')
      sigma = np.zeros(nt,dtype='float64')
      for t in range(nt):
          x_avg[t] = np.sum(x[:,t])/n
          sigma_x2[t] = np.sum(np.power(np.add(x[:,t], - x_avg[t]),2))
          sigma[t] = np.sqrt(sigma_x2[t])*np.sqrt((n-1.)/n)
  return x_avg,sigma

cpdef jack_resample(array_t x):
  #jackknife resampling
  cdef Py_ssize_t n = x.shape[0]
  cdef array_t x_J = np.zeros_like(x)
  cdef Py_ssize_t iextr, i
  for iextr in range(n):
    for i in range(n):    
        if iextr != i:
            x_J[iextr] = x_J[iextr] + x[i]
  x_J=x_J/(n-1.)
  return x_J

cpdef jack_resample_Nt(array2_t x):
  #jackknife resampling
  cdef Py_ssize_t n = x.shape[0]
  cdef Py_ssize_t nt = x.shape[1]

  cdef array2_t x_J = np.zeros_like(x)
  cdef Py_ssize_t iextr, i, t
  for iextr in range(n):
    for i in range(n):    
        if iextr != i:
            for t in range(nt):
                x_J[iextr,t] = x_J[iextr,t] + x[i,t]/float(n-1.)
  return x_J

cpdef jack_statistics(scalar_t x_cv, array_t x_J):
  #full jackknife statistics
  cdef Py_ssize_t n = x_J.shape[0]
  theta, sig = jack_sigma(x_J)
  bar_x = n*x_cv - (n-1)*theta 
  return bar_x, sig

cpdef correlation_coefficient(array_t x, array_t y):
    z = np.multiply(x,y)
    cdef Py_ssize_t n = z.shape[0]
    sumxy = np.sum(z)
    sumx = np.sum(x)
    sumy = np.sum(y)
    sumx2 = np.sum(np.power(x,2))
    sumy2 = np.sum(np.power(y,2))
    nomin=(n*sumxy - sumx*sumy)
    denomin=(np.sqrt((n*sumx2 - sumx**2)*(n*sumy2 - sumy**2)))
    rho = nomin/denomin
    return rho




