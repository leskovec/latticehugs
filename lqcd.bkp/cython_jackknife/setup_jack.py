from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

#run with: LDSHARED="icc -shared" CC="icc -xhost" python setup.py build_ext --inplace
setup(
  name = 'c_jack',
  ext_modules=[
    Extension('c_jackknife', ['c_jackknife.pyx'],include_dirs=[numpy.get_include()])
    ],
  cmdclass = {'build_ext': build_ext}
)
