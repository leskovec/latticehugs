import numpy as np
from decimal import *
import subprocess


#######################                auxiliary functions                #######################


def fit_eigenvalue(type,t0,tmin_fit, tmax_fit, lam, cov_loc, p_init):
	#################################################################################################################
	#this version will now use sasa's meson_mass_fact, which i will have to recode by myself eventually, so that i
	#can use it in python

	nt_exc=4000
	fact_exc=1
	eps=0.0000001
	max_iter=2000
	verbose=0
	held_fixed=0
		
	if type == '2exp':

		f_init=p_init[0]
		en_init=p_init[1]
		f_exc_init=p_init[2]
		en_exc_init=p_init[3]

		#for mac os X
		command="{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15}".format("echo", str(nt_exc), str(fact_exc), str(eps), str(max_iter), str(verbose), str(tmin_fit), str(tmax_fit), str(held_fixed), str(f_init), str(en_init), str(f_exc_init), str(en_exc_init), " | /Users/leskovec/Dropbox/fit_codes/bin/meson_mass_fact.exe ", str(cov_loc),  "2>/dev/null ")
	
		#for linux
		#command="{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13} {14} {15}".format("echo", str(nt_exc), str(fact_exc), str(eps), str(max_iter), str(verbose), str(tmin_fit), str(tmax_fit), str(held_fixed), str(f_init), str(en_init), str(f_exc_init), str(en_exc_init), " | /Users/leskovec/Dropbox/fit_codes/bin/meson_mass_fact_linx.exe ", str(cov_loc),  "2>/dev/null ")
	

		proc = subprocess.Popen(str(command), stdout=subprocess.PIPE,shell=True)
		out,err=proc.communicate()

		file=open('error_log','w')
		file.write(out)
		file.close()

		loc_p0=out.find("par[0] =",0)
		loc_p1=out.find("par[1] =",0)
		loc_p2=out.find("par[2] =",0)
		loc_p3=out.find("par[3] =",0)

		loc_chi_square=out.find("chi-square",0)

		p0_i=float(out[loc_p0+9:loc_p0+22])
		p1_i=float(out[loc_p1+9:loc_p1+22])
		p2_i=float(out[loc_p2+9:loc_p2+22])
		p3_i=float(out[loc_p3+9:loc_p3+22])
		c2=float(out[loc_chi_square+40:loc_chi_square+52])
		dof=int(out[loc_chi_square+15:loc_chi_square+18])

		p1=min(p1_i,p3_i)
		p3=max(p1_i,p3_i)

		p0=max(p0_i,p2_i)
		p2=min(p0_i,p2_i)

		popt = []
		popt.append(p0)
		popt.append(p1)
		popt.append(p2)
		popt.append(p3)

	elif type == '1exp':

		f_init=p_init[0]
		en_init=p_init[1]

		#for mac os X
		command="{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10} {11} {12} {13}".format("echo", str(nt_exc), str(fact_exc), str(eps), str(max_iter), str(verbose), str(tmin_fit), str(tmax_fit), str(held_fixed), str(f_init), str(en_init), " | /Users/leskovec/Dropbox/fit_codes/bin/meson_1exp.exe ", str(cov_loc),  "2>/dev/null ")
	
		proc = subprocess.Popen(str(command), stdout=subprocess.PIPE,shell=True)
		out,err=proc.communicate()

		file=open('error_log','w')
		file.write(out)
		file.close()

		loc_p0=out.find("par[0] =",0)
		loc_p1=out.find("par[1] =",0)

		loc_chi_square=out.find("chi-square",0)

		p0=float(out[loc_p0+9:loc_p0+22])
		p1=float(out[loc_p1+9:loc_p1+22])
		c2=float(out[loc_chi_square+40:loc_chi_square+52])
		dof=int(out[loc_chi_square+15:loc_chi_square+18])


		popt = []
		popt.append(p0)
		popt.append(p1)


	#################################################################################################################
	return popt,c2,dof

def fit_eigenvalue_cbl(pinit,tmin_fit, tmax_fit, jack_loc, avg_loc, ijack_loc):
	### probationary cbl fitter use		

	command="{0} {1} {2} {3} {4} {5} {6} {7} {8} {9}".format("/Users/leskovec/Dropbox/cbl_fitter/a.out", str(jack_loc), str(avg_loc), str(ijack_loc), str(tmin_fit+1), str(tmax_fit+1), str(pinit[0]), str(pinit[1]), str(pinit[2]), str(pinit[3]))

	proc = subprocess.Popen(str(command), stdout=subprocess.PIPE,shell=True)
	out,err=proc.communicate()

	dof=tmax_fit - tmin_fit - 4 + 1

	c2=float(out[32:42])*dof
	p0=float(out[52:62])
	p1=float(out[92:102])
	p2=float(out[132:142])
	p3=float(out[172:182])

	popt = []
	popt.append(p0)
	popt.append(p1)
	popt.append(p2)
	popt.append(p3)

	#################################################################################################################
	return popt,c2,dof

def fit_Zfactor(t0,tmin_fit, tmax_fit, cov_Z_loc, p_init):
	#################################################################################################################
	#this version will use linear from fit_codes package

	nt_exc=16712
	fact_exc=1
	eps=0.00000001
	max_iter=1000
	verbose=0
	held_fixed=0
	Z_init=p_init[0]

	#for mac os X
	command="{0} {1} {2} {3} {4} {5} {6} {7} {8} {9} {10}".format("echo", str(eps), str(max_iter), str(verbose), str(tmin_fit), str(tmax_fit), str(held_fixed), str(Z_init), " | /Users/leskovec/Dropbox/fit_codes/bin/constant.exe", str(cov_Z_loc), "2>/dev/null")
	

	proc = subprocess.Popen(str(command), stdout=subprocess.PIPE,shell=True)
	out,err=proc.communicate()

	file=open('error_log','w')
	file.write(out)
	file.close()

	loc_p0=out.find("par[0] =",0)

	loc_chi_square=out.find("chi-square",0)

	p0_i=float(out[loc_p0+9:loc_p0+22])
	c2=float(out[loc_chi_square+40:loc_chi_square+52])
	dof=int(out[loc_chi_square+15:loc_chi_square+18])

	p0=p0_i

	popt = []
	popt.append(p0)

	#################################################################################################################
	return popt,c2,dof



def phase(typed,l,m,d,m1,m2,Nl,q2,E):
	#only for the A1 and T1 irrep
	#only for cont dispersion relation
	#input should look like this:
	#l m d[0] d[1] d[2] m1     m2    Nl 5 q
	#0 0 0    0    0    0.1673 0.334 32 8 0.4

	file=open('zeta_tmp','w')
	file.write('{0} {1} {2} {3} {4} {5} 6 {6}'.format(l,m,str(d).translate(None,'[],'),m1,m2,Nl,q2))
	file.close()
	proc = subprocess.check_output('./get_zeta < zeta_tmp', shell=True)
	tt=proc.find('integrate')
	if (tt != -1 ):
		phase=0
	else:
		val=proc.split()
		re_zeta00=float(val[1])
		im_zeta00=float(val[2])
		zeta00=re_zeta00+im_zeta00*1j

		tmp1=np.sqrt(np.power(np.pi,3))
		gam=gamma(typed,E,d,Nl)
		if (q2 >= 0):
			q=np.sqrt(q2)  #note: only for q2>=0 
		else:
			q=np.sqrt(-q2)*1j #note: this doesnt take care of it, just removes the errors

		tmp=tmp1*gam*q/zeta00

		phase_rad=np.arctan(tmp)
		phase=phase_rad*180./np.pi
		if phase < 0:
			phase+=180
	return phase

def check_deltas(j_q2_avg, j_delta_avg, ja_delta,Nkonf):
	test=0
	for i in range(Nkonf):
		if (ja_delta[i] - j_delta_avg) >= 90:
			print "huge jump in delta values -- jackknife error of phase shift incorrect"
			test+=1
	return test





def BW_lin(x,A,B):
	return A-B*np.power(x,1)

def fit_BW_lin(s,prod):
	BW_p,BW_pcov=curve_fit(BW_lin,s,prod,p0=(2.,2.15))
	g2=6*np.pi/BW_p[1]
	mR2=BW_p[0]/BW_p[1]
	g=np.sqrt(g2)
	mR=np.sqrt(mR2)
	return mR,g
