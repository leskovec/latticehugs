'''
  This python module contains the read routines for the hdf5 gevp format as well
  as the old ascii format
'''

import h5py
import numpy as np

def set_scale(a,da):
  #using NIST data for hbar and c
  #hbar[eV] 6.582 119 514 x 10-16 eV s
  #speef of light [m/2] 299 792 458 m s-1
  #spacing must be in fermi
    hbarc=197.32697879518255
    scale=hbarc/a
    rel_err_scale=(da/a)
    return scale,rel_err_scale
 
def read_cnfg_list(infile):
  #read list of configurations used in this analysis
  file_gevp=h5py.File(infile, 'r')
  conf_list=file_gevp['config list']['configuration list']
  return conf_list

#central values
def read_eigenvalue_cv(infile,t0,idx):
  file_gevp=h5py.File(infile, 'r')
  s_lamba='lambda '+str(idx)
  s_t0='t0_'+str(t0)
  eigenvalue=file_gevp[s_t0]['central value']['eigenvalue [lambda]'][s_lamba]
  return eigenvalue

def read_eigenvalue_effmass_cv(infile,t0,idx):
  file_gevp=h5py.File(infile, 'r')
  s_meff='meff '+str(idx)
  s_t0='t0_'+str(t0)
  meff=file_gevp[s_t0]['central value']['eigenvalue effective mass [meff-lambda]'][s_meff]
  return meff

def read_opt_correlator_cv(infile,t0,idx):
  file_gevp=h5py.File(infile, 'r')
  s_uCu='optimized correlator '+str(idx)
  s_t0='t0_'+str(t0)
  uCu=file_gevp[s_t0]['central value']['optimized correlator [uCu]'][s_uCu]
  return uCu

def read_opt_correlator_effmass_cv(infile,t0,idx):
  file_gevp=h5py.File(infile, 'r')
  s_meffuCu='meff '+str(idx)
  s_t0='t0_'+str(t0)
  meffuCu=file_gevp[s_t0]['central value']['optimized correlator effective mass [meff-uCu]'][s_meffuCu]
  return meffuCu

def read_uvec_cv(infile,t0,idx,basis):
  file_gevp=h5py.File(infile, 'r')
  s_t0='t0_'+str(t0)
  arr=file_gevp[s_t0]['central value']['generalized eigenvector [u]']
  uvec={}
  for p in basis:
    s_r_uvec='real - u '+str(p) +' [operator] '+str(idx)+' [state]'
    s_i_uvec='imag - u '+str(p) +' [operator] '+str(idx)+' [state]'
    r_uvec=arr[s_r_uvec]
    i_uvec=arr[s_i_uvec]
    tmp_r_uvec=np.array(r_uvec)
    tmp_i_uvec=np.array(i_uvec)
    tmp_uvec = tmp_r_uvec + tmp_i_uvec*1j
    uvec[int(p)]=tmp_uvec
  return uvec

def read_uvec_sig(infile,t0,idx,basis):
  file_gevp=h5py.File(infile, 'r')
  s_t0='t0_'+str(t0)
  arr=file_gevp[s_t0]['standard deviation']['generalized eigenvector [u]']
  uvec={}
  for p in basis:
    s_r_uvec='real - u '+str(p) +' [operator] '+str(idx)+' [state]'
    s_i_uvec='imag - u '+str(p) +' [operator] '+str(idx)+' [state]'
    r_uvec=arr[s_r_uvec]
    i_uvec=arr[s_i_uvec]
    tmp_r_uvec=np.array(r_uvec)
    tmp_i_uvec=np.array(i_uvec)
    tmp_uvec = tmp_r_uvec + tmp_i_uvec*1j
    uvec[int(p)]=tmp_uvec
  return uvec

def read_Cu_cv(infile,t0,idx,basis):
  file_gevp=h5py.File(infile, 'r')
  s_t0='t0_'+str(t0)
  arr=file_gevp[s_t0]['central value']['overlap factor Cu [Cu]']
  Zvec={}
  for p in basis:
    s_r_Zvec='real - Z '+str(p) +' [operator] '+str(idx)+' [state]'
    s_i_Zvec='imag - Z '+str(p) +' [operator] '+str(idx)+' [state]'
    r_Zvec=arr[s_r_Zvec]
    i_Zvec=arr[s_i_Zvec]
    tmp_r_Zvec=np.array(r_Zvec)
    tmp_i_Zvec=np.array(i_Zvec)
    tmp_Zvec = tmp_r_Zvec + tmp_i_Zvec*1j
    Zvec[int(p)]=tmp_Zvec
  return Zvec

def read_Cu_sig(infile,t0,idx,basis):
  file_gevp=h5py.File(infile, 'r')
  s_t0='t0_'+str(t0)
  arr=file_gevp[s_t0]['standard deviation']['overlap factor Cu [Cu]']
  Zvec={}
  for p in basis:
    s_r_Zvec='real - Z '+str(p) +' [operator] '+str(idx)+' [state]'
    s_i_Zvec='imag - Z '+str(p) +' [operator] '+str(idx)+' [state]'
    r_Zvec=arr[s_r_Zvec]
    i_Zvec=arr[s_i_Zvec]
    tmp_r_Zvec=np.array(r_Zvec)
    tmp_i_Zvec=np.array(i_Zvec)
    tmp_Zvec = tmp_r_Zvec + tmp_i_Zvec*1j
    Zvec[int(p)]=tmp_Zvec
  return Zvec

#jackknife
def read_eigenvalue_jack(infile,t0,idx):
  #shape: (50.96)
  file_gevp=h5py.File(infile, 'r')
  s_lamba='lambda '+str(idx)
  s_t0='t0_'+str(t0)
  eigenvalue=file_gevp[s_t0]['jackknife']['eigenvalue [lambda]'][s_lamba]
  return eigenvalue

def read_eigenvalue_effmass_jack(infile,t0,idx):
  file_gevp=h5py.File(infile, 'r')
  s_meff='meff '+str(idx)
  s_t0='t0_'+str(t0)
  meff=file_gevp[s_t0]['jackknife']['eigenvalue effective mass [meff-lambda]'][s_meff]
  return meff

def read_opt_correlator_jack(infile,t0,idx):
  file_gevp=h5py.File(infile, 'r')
  s_uCu='optimized correlator '+str(idx)
  s_t0='t0_'+str(t0)
  uCu=file_gevp[s_t0]['jackknife']['optimized correlator [uCu]'][s_uCu]
  return uCu

def read_opt_correlator_effmass_jack(infile,t0,idx):
  file_gevp=h5py.File(infile, 'r')
  s_meffuCu='meff '+str(idx)
  s_t0='t0_'+str(t0)
  meffuCu=file_gevp[s_t0]['jackknife']['optimized correlator effective mass [meff-uCu]'][s_meffuCu]
  return meffuCu

def read_uvec_jack(infile,t0,idx,basis):
  file_gevp=h5py.File(infile, 'r')
  s_t0='t0_'+str(t0)
  arr=file_gevp[s_t0]['jackknife']['generalized eigenvector [u]']
  uvec={}
  for p in basis:
    s_r_uvec='real - u '+str(p) +' [operator] '+str(idx)+' [state]'
    s_i_uvec='imag - u '+str(p) +' [operator] '+str(idx)+' [state]'
    r_uvec=arr[s_r_uvec]
    i_uvec=arr[s_i_uvec]
    tmp_r_uvec=np.array(r_uvec)
    tmp_i_uvec=np.array(i_uvec)
    tmp_uvec = tmp_r_uvec + tmp_i_uvec*1j
    uvec[int(p)]=tmp_uvec
  return uvec

def read_Cu_jack(infile,t0,idx,basis):
  file_gevp=h5py.File(infile, 'r')
  s_t0='t0_'+str(t0)
  arr=file_gevp[s_t0]['jackknife']['overlap factor Cu [Cu]']
  Zvec={}
  for p in basis:
    s_r_Zvec='real - Z '+str(p) +' [operator] '+str(idx)+' [state]'
    s_i_Zvec='imag - Z '+str(p) +' [operator] '+str(idx)+' [state]'
    r_Zvec=arr[s_r_Zvec]
    i_Zvec=arr[s_i_Zvec]
    tmp_r_Zvec=np.array(r_Zvec)
    tmp_i_Zvec=np.array(i_Zvec)
    tmp_Zvec = tmp_r_Zvec + tmp_i_Zvec*1j
    Zvec[int(p)]=tmp_Zvec
  return Zvec

#TODO
#note the pion has a relativistic dispersion relation, so the code below is not really needed,
#but eventually a h5 file with all the configs and fitted masses should be present here
#
#def read_disp(lattice,meson,list_konf,Nkonf):
#  print lattice,meson
#  if lattice == 'anna':
#    if meson == 'D':
#      m1_loc="/Users/leskovec/Dropbox/anna_disp/disp_D_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/anna_disp/disp_D_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/anna_disp/disp_D_M4.dat"
#    elif meson == 'D*':
#      m1_loc="/Users/leskovec/Dropbox/anna_disp/disp_Dstar_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/anna_disp/disp_Dstar_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/anna_disp/disp_Dstar_M4.dat"
#    elif meson == 'etac':
#      m1_loc="/Users/leskovec/Dropbox/anna_disp/disp_etac_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/anna_disp/disp_etac_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/anna_disp/disp_etac_M4.dat"
#    elif meson == 'jpsi':
#      m1_loc="/Users/leskovec/Dropbox/anna_disp/disp_jpsi_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/anna_disp/disp_jpsi_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/anna_disp/disp_jpsi_M4.dat"
#    elif meson == 'spav':
#      m1_loc="/Users/leskovec/Dropbox/anna_disp/disp_cc_spav_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/anna_disp/disp_cc_spav_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/anna_disp/disp_cc_spav_M4.dat"
#  elif lattice == 'pacs-cs':
#    if meson == 'D':
#      m1_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_D_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_D_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_D_M4.dat"
#    elif meson == 'D*':
#      m1_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_Dstar_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_Dstar_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_Dstar_M4.dat"
#    elif meson == 'etac':
#      m1_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_etac_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_etac_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_etac_M4.dat"
#    elif meson == 'jpsi':
#      m1_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_jpsi_M1.dat"
#      m2_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_jpsi_M2.dat"
#      m4_loc="/Users/leskovec/Dropbox/pacs-cs_disp/disp_jpsi_M4.dat"
#
#  f_m1=open(m1_loc,"rU")
#  f_m2=open(m2_loc,"rU")
#  f_m4=open(m4_loc,"rU")
#
#  disp_m1=np.zeros((Nkonf),dtype='float64')
#  disp_m2=np.zeros((Nkonf),dtype='float64')
#  disp_m4=np.zeros((Nkonf),dtype='float64')
#
#  line_m1=f_m1.readline()
#  line_m1=line_m1.strip()
#  cols_m1=line_m1.split()
#  
#  cv_disp_m1=float(cols_m1[1])
#
#  line_m2=f_m2.readline()
#  line_m2=line_m2.strip()
#  cols_m2=line_m2.split()
#  
#  cv_disp_m2=float(cols_m2[1])
#
#  line_m4=f_m4.readline()
#  line_m4=line_m4.strip()
#  cols_m4=line_m4.split()
#  
#  cv_disp_m4=float(cols_m4[1])
#
#  for konf in range(Nkonf):
#    line_m1=f_m1.readline()
#    line_m1=line_m1.strip()
#    cols_m1=line_m1.split()
#    config=int(cols_m1[0])
#    list_konf_index=int(np.where(list_konf==config)[0])
#    disp_m1[list_konf_index]=float(cols_m1[1])
#
#    line_m2=f_m2.readline()
#    line_m2=line_m2.strip()
#    cols_m2=line_m2.split()
#    config=int(cols_m2[0])
#    list_konf_index=int(np.where(list_konf==config)[0])
#    disp_m2[list_konf_index]=float(cols_m2[1])
#
#    line_m4=f_m4.readline()
#    line_m4=line_m4.strip()
#    cols_m4=line_m4.split()
#    config=int(cols_m4[0])
#    list_konf_index=int(np.where(list_konf==config)[0])
#    disp_m4[list_konf_index]=float(cols_m4[1])
#
#  f_m1.close()
#  f_m2.close()
#  f_m4.close()
#
#  return cv_disp_m1,cv_disp_m2,cv_disp_m4,disp_m1,disp_m2,disp_m4
#
